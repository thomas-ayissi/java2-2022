<!DOCTYPE html>
<html>

<head>
    <title>202: Collections API</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <!-- Font Awesome -->
    <script src="js/79b3831d6d.js" crossorigin="anonymous"></script>
    <style>
    .cssClass > rect{
        stroke:#FFFF00;
        stroke-width:4px;
    }
</style>
</head>

<body>
    <textarea id="source">

class: center, middle
<i class="fas fa-box-open" style="font-size: 7em;"></i>

# Java Collections API

#### <i class="fab fa-java"></i> Java 2 - ISEN - 2022

---

## Introduction

--

The *Collections* API has been introduced in the JDK 1.2.

--

Since the JDK 1.5, it has been rewritten to use *generic types*.

--

*Collections* are groups of *objects*.

--

The API has several *Interfaces* and implementation *classes*.

---

## Interfaces *(some)*

--

<div class="mermaid center" style="height: 25em;">
classDiagram
    class Collection
    class Map
    class Iterator
    class List
    class Set
    class SortedMap
    class ListIterator
    class SortedSet
    class Iterable
    class ConcurrentMap
    Collection <|-- List
    Collection <|-- Set
    Set <|-- SortedSet
    Map <|-- SortedMap
    Map <|-- ConcurrentMap
    Iterable <|-- Collection
    Iterator <|-- ListIterator
</div>

---

## Collections & Generic types

--

All the *interfaces* and *classes* of the *Collection* API use heavily the *generic types*.

--

All the elements in the *collection* have a certain *type*.

--

Only a *type* of *object* can be added in a *collection*.

---

## Iterable interface

--

All *collections* extend the `Iterable` *interface*.

--

An *object* that implements the `Iterable` *interface* can be used in a *for-each* loop.

```java
for(String `element` : collection) { // collection is being browsed

    System.out.println(`element`);  // element is the current iteration

}
```

---

## Collection interface

--

The `Collection` *interface* describes a simple group of *objects*.

--

It is possible to :

--

* add and remove *objects*;

--

* test if an *object* is in the *collection*;

--

* count the number of *objects* in the *collection*;

--

* iterate through the *collection’s* *objects*.

---

## Principal methods of a collection

Method|Description
:---|:---
`int size()`|Get the number of elements
`boolean isEmpty()`|true if no element
`boolean add(E element)`|Add the element
`boolean remove(E element)`|Remove the element
`void clear()`|Remove all the elements
`Iterator<E> iterator()`|Get an iterator on the list elements
`boolean contains(E element)`|true if element is in the collection

---

## An example

```java
Collection<String> collectionOfStrings = ... ;

boolean isEmpty = collectionOfStrings.isEmpty(); // returns true

collectionOfStrings.add("Hello");

collectionOfStrings.add("World");

int size = collectionOfStrings.size(); // returns 2

collectionOfStrings.remove("Hello");

boolean containsWorld = collectionOfStrings.contains("World"); // returns true

boolean containsHello = collectionOfStrings.contains("Hello"); // returns false

collectionOfStrings.clear();

isEmpty = collectionOfStrings.isEmpty(); // returns true
```

---

## Reading elements of a collection

--

With an `Iterator`, it is possible to process the elements of a *collection* one by one.

--

The `Iterator` *interface* has two main *methods*:

Method|Description
---|---
`E next()`|Get the next element of the iterator
`boolean hasNext()`|true if there is an element after the current one

---

## Using an iterator

--

`Iterator` can be used as follows :

```java
Iterator<String> iterator = collection.iterator();

while(iterator.hasNext()) {
    String element = `iterator.next()`;
    System.out.println(element);
}
```

--

This is the same as with the `Iterable` interface :

--

```java
for(String element : collection) {

    System.out.println(element);

}
```

--

> so why use an iterator ?

---

## Removing an element...

--

It is *not* possible to remove an element from a *collection* inside a *for-each* loop.

--

To remove an element while reading the elements of a *collection*, it is necessary to use an *iterator* and its method: `remove()`

--

```java
Iterator<String> iterator = collectionOfStrings.iterator();

while (iterator.hasNext()) {
    String currentElement = iterator.next();
    // Do something
    iterator.remove(); //Removes the current element
}
```

---

## ...using a functional interface

--

A *functional interface* is an interface which has a single abstract method, called *functional method*.

--

It is designed to process a specific function and are used as arguments in other functions. They are implemented by *anonymous classes*.

--

E.g. removing an element with a `Predicate` :

```java
Interface Predicate<T> { // returns a boolean according to a given predicate
    boolean test(T t)
}
```

```java
filteredList.removeIf(new Predicate<T>() {
    @Override
    public boolean test(T t) {
        // compute remove condition
        return myRemoveCondition; //Removes the t element
    }
}
```

---

## List interface

--

The `List` *interface* extends a `Collection` by adding an index system.

--

Elements added to a `List` are assigned to an index.

--

An *iterator* or a *for-each* loop on a `List` will always get its elements **in the same order**

---

## Methods of a list

Method|Description
---|---
`E get(int index)`|Get the element at the index
`E set(int index, E element)`|Change the element at the index and return the old one
`void add(int index, E element)`|Insert the element at the index
`E remove(int index)`|Remove the element and returns it
`int indexOf(E element), int lastIndexOf(E element)`| Get the first or last index of the element
`List<E> subList(int start,int end)`|Extract a sub list between two indexes
---

## ListIterator interface

--

A `listIterator()` method of a `List` creates a `ListIterator`.

--

A `ListIterator` can browse a `List` forward and backward.

Method|Description
---|---
`E previous()`|Get the previous element of the iterator
`boolean hasPrevious()`|true if there is an element before the current one

---

## ArrayList class

--

`ArrayList` is the most common used implementation of the `List` *interface*.

--

Internally, it uses an array to store the elements.

--

```java
List<String> listOfStrings = new ArrayList<>();

List<String> listOfStrings = new ArrayList<>(10)
```

---

## LinkedList class

--

`LinkedList` is an other implementation of the `List` *interface*.

--

Internally, a linked list is used to store the elements.

```java
List<Integer> listOfIntegers = new LinkedList<>()
```

---

## Set interface

--

A `Set` is a collection where there can’t be any duplicates.

--

The `.equals()` method of the element is used to check duplication.

--

The methods accessible to a `Set` come from the `Collection` *interface*, not from the `List` *interface*

---

## Set implementations

--

`HashSet` and `LinkedHashSet` implement the `Set` *interface*.

--

Both these *classes* use a hash to store the elements

--

```java
Set<String> setOfStrings = new HashSet<>();

Set<Integer> setOfIntegers = new LinkedHashSet<>()
```

---

## SortedSet interface

--

A `SortedSet` extends a `Set` and add sorting between the elements of the collection.

--

When adding a new element in a `SortedSet`, it is inserted at the right place, i.e according to the sorting algorithm used.

--

Consequently, elements in a `SortedSet` must be compared in order to sort them.

--

There are two ways to compare two object of the same class:

--

* By implementing the `Comparable<E>` interface;

--

* By using a `Comparator<E>`

---

## Comparable interface

--

The `Comparable` *interface* requires a *class* to implement the `compareTo(E)` method.

--

The `compareTo(E)` method returns an int:

--

* *0* if both values are equals;

--

* *a negative value* if the current object is smaller than the parameter

--

* *a positive value* if the current object is bigger than the parameter

--

```java
public class Human implements `Comparable<Human>` {

        private Integer size;

        `@Override`
        `public int compareTo(Human other) {`
            `return size.compareTo(other.getSize());`
        `}`

}
```

---

## Using a Comparator

--

`Comparator` is a *functional interface* to compare two objects without modifying the class.

--

It can be used as an *anonymous class*:

--

```java
Comparator<String> comparator = new `Comparator<String>()` {

    `@Override`
    `public int compare(String o1, String o2) {`
    // Find a new way to compare strings
    `}`

}
```

---

## Methods of a SortedSet

--

A `SortedSet` adds two methods to access elements from the Collection:

Method|Description
---|---
`E first()`|Get the smallest element of the collection
`E last()`|Get the biggest element of the collection
`Comparator<E> comparator()`|Get the comparator used if any is used
---

## TreeSet class

--

The `TreeSet` class implements the `SortedSet` interface.

--

The elements are stored in a Red-Black tree.

```java
SortedSet sortedSet = new TreeSet<>();

SortedSet sortedSet2 = new TreeSet<>(comparator)
```

???

A red-black tree is a tree with O log(n) guaranty for search, insert and delete operations

---

## Brief summary of collections

Implementation|Indexed|Sorted|Unicity|
---|:---:|:---:|:---:
ArrayList|X||
LinkedList|X||
HashSet|||X
TreeSet||X|X

---

## Map interface

--

A `Map` is a different kind of group of objects.

--

It is a group of Key-value pairs.

--

Both the key and the value have its own *generic type*.

```java
Map<`String`, `Human`> mapOfHumans;
```

---

## Adding and get elements

--

The methods to add, get and remove elements use the key:

Method|Description
---|---
`V put(K key, V value)`|Add the value to the map and associates it with the key. Returns the old value associated with the key if any.
`V get(K key)`|Get the value associated to the key
`V remove(K key)`|Remove the the value associated to the key and returns it if any

---

## Map and collections

--

A `Map` is **not** a `Collection`!

--

* It does not extends the `Collection` *interface*.

--

* It is not iterable.

--

A `Map` regroups several collections:

--

* A `Set` of keys

--

* A `Collection` of values

--

* A `Set` of key-value pairs called `Entry`

---

## Access to the collections

--

A `Map` has several methods to access the sub-collections:

Method|Description
---|---
`Set<K> keySet()`|Returns a Set of the map keys
`Collection<V> values()`|Returns a Collection of the map values
`Set<Entry<K,V>> entrySet()`|Returns a Set of key-value pairs

---

## Some other methods

Method|Description
---|---
`void clear()`|Empties the map
`int size()`|Get the number of key-value pairs
`boolean isEmpty()`|Returns true if the size is equal to 0
`boolean containsKey(K key)`|Return true if the key is in the map
`boolean containsValue(V value)`|Return true if the value is in the map

---

## Map implementations

--

`HashMap` and `LinkedHashMap` implement the `Set` *interface*.

--

They are the map equivalent of the `Set` implementations `HashSet` and `LinkedHashSet`.

```java
Map<String, Human> mapOfHumans = new HashMap<>();

Map<String, Dodo> mapOfDodos = new LinkedHashMap<>()
```

---

## SortedMap interface

--

A `SortedMap` is a `Map` with a `SortedSet` of keys.

--

It is to `Map` what `SortedSet` is to `Set`.

--

The key generic type must be `Comparable` or a `Comparator` must be used

---

## TreeMap class

--

The `TreeMap` class is the implementation of `SortedMap`.

--

It is an equivalent of `TreeSet` for `Map`.

```java
SortedMap<String, Human> sortedMap = new TreeMap<>();

SortedMap<String, Human> sortedMap = new TreeMap<>(comparator)
```

---

## Collections class

--

Collection**s** is a utility class with a lot of static methods to interact with collections.

--

There are a lot of methods so we will only list some of them, look at the JDK documentation for the others

--

Method|Description
---|---
`boolean addAll(Collection<? super E> c, E... elements)`|Add all elements to the collection
`int frequency(Collection<E> collection, E element)`|Returns how many time the element is in the collection.
`E min(Collection<E> coll), E max(Collection<E> coll)`|Returns the min or max element of the collection. Can also use a comparator.
`void sort(List<E> list)`|Sort a list. Can also use a comparator.
`void reverse(List<E> list)`|Reverse the order of the elements in the list.
`void shuffle(List<E> list)`|Shuffle the order of the elements in the list.
`void swap(List<E> list, int i, int j)`|Swap the two elements at the specific indexes of the list

---

## Collections.emptyList()

--

One last trick of `Collections` for `List` instantiation:

--

Sometimes you will want an empty `List` instance, for example for testing purposes.

--

`new ArrayList()` does the job, but creates a flawed object, for two reasons:

--
  * it is mutable, so you can end up with actually something in your list

--
  * it allocates memory for nothing

--

You can instead use `Collections.emptyList()`, which solves the two problems:

--
  * It is immutable

--
  * it is the same object throughout the JVM

--

> Good news! it also works for `emptySet()` and `emptyMap()`

---

## Collections.addAll()

--

A cumbersome limitation of the Collections API is that most class constructors take another collection as an argument. This make e.g. Lists creation a little tedious

```java
var list = new ArrayList<String>();
list.add("foo");
list.add("bar");
list.add("2000");
...
```

--

Collections partially solves this problem:

--

```java
var myList = new ArrayList<String>();
Collections.addAll(myList,"foo", "bar", "2k");
```

--

Sadly, it does not work with `Map` ...

---

## Immutable Factory methods

--

To address part of this problem, Java 9 introduces immutable Collections factory methods:

--

```java
List<String> list = List.of("foo", "bar", "2k");
Set<String> set = Set.of("foo", "bar", "2k");
Map<String, String> map = Map.of("foo", "a", "bar", "b", "baz", "c");
```

--

Several advantages:

--
  * they are semantically explicit, thus more readable

--
  * they are truly immutable (`Arrays.asList` is not...)

--
  * their implementations are more efficient memorywise

--
  * it works with `Map`, too

--
  * You can even use them in constructors:

```java
Map<String, String> map2 = new HashMap<String, String> (
  Map.ofEntries(
    new AbstractMap.SimpleEntry<String, String>("name", "John"),
    new AbstractMap.SimpleEntry<String, String>("city", "budapest")));
```

---

## To sum up

Depending on the use of your collection, consider these methods:

* Immutable collection, whatever the structure (e.g. for testing purposes): `List.of()`

* Mutable Collection : regular constructor + the previous immutable collection, or `Collections.addAll()`

---

## .hashCode() and .equals()

--

How to know if an *object* is already present in a `Set` ?

--

How to know if a List contains a particular *object* ?

---

## .equals()

--

A simple method inherited from the `Object` class.

--

It takes another *object* and returns a `boolean`

--

* true if this other *object* is **considered equal** to the current instance.

--

* false otherwise

---

## .equals() is not "=="

```java
String string1= new String("test");

String string2= new String("test");

boolean isSame = (string1 == string2);

System.out.println(isSame);//prints false

boolean isEqual = (string1.equals(string2));

System.out.println(isEqual);//prints true
```

--

## But by default it is

The equals method for *class* `Object` implements the most discriminating possible equivalence relation on objects;

--

that is, for any *non-null* reference values x and y, this method returns `true` if and only if x and y refer to the same object (x == y has the value `true`)

---

## .hashCode()

--

An other simple method inherited from the `Object` class.

--

It takes no arguments and returns an `Integer`, computed thanks to the values of the attributes

---

## .hashCode() and .equals()

--

If you redefine `.equals()`, you **must** redefine `.hashCode()` and vice versa!

--

If two objects are considered equal, they must have the same hashCode and vice versa.

--

Please use your IDE to generate these methods

--

Usage of `.hashCode()` and `.equals()` in collections API

--

* `.equals()` is used when `.contains()`, `.indexOf()` or other methods like .`.remove()` are called on a List

--

* `HashMap` or `HashSet` have an implementation that relies on the `.hashCode()` method, in order to register data in memory

---

## Let's recap what we've seen

--

Collections and it's children, Map and it's children,

--

Functional interfaces

--

For-each loop

--

`Iterator` functional interface

--

`Comparable` functional interface

---

class: center, middle
<i class="fas fa-play" style="font-size: 7em;"></i>

# now let's have some fun

### with [practical work #2](../pw02/assignment/Java2_PW02_library/)

---

## Sources

--

<https://docs.oracle.com/javase/tutorial/java/generics/>

--

<http://www.jmdoudoux.fr/java/dej/chap-techniques_java.htm#techniques_java-2>

        </textarea>

    <script src="js/mermaid.min.js"></script>
    <script src="js/remark-latest.min.js"></script>
    <script src="js/slideshow.js"></script>
    <script>
   mermaid.initialize({
    startOnLoad: false,
    cloneCssStyles: false,
    theme: 'dark',
    class:{
        useMaxWidth: false,
    }
   });
   function initMermaid(s) {
      var diagrams = document.querySelectorAll(".mermaid");
      var i;
      for (i = 0; i < diagrams.length; i++) {
         if (diagrams[i].offsetWidth > 0) {
         mermaid.init(undefined, diagrams[i]);
         }
      }
   }
   slideshow.on("afterShowSlide", initMermaid);
   initMermaid(slideshow.getSlides()[slideshow.getCurrentSlideIndex()]);
  </script>
</body>
</html>
