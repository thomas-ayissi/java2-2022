<!DOCTYPE html>
<html>

<head>
   <title>401: Unit tests</title>
   <meta charset="utf-8">
   <link rel="stylesheet" type="text/css" href="./css/style.css">
   <script src="https://kit.fontawesome.com/79b3831d6d.js" crossorigin="anonymous"></script>
   <link rel="stylesheet" type="text/css" href="./css/nerd-fonts-generated.min.css">
</head>

<body>
   <textarea id="source">

name: inverse
class: center, middle
<i class="fas fa-question" style="font-size: 7em;"></i>

# Unit tests

#### <i class="fab fa-java"></i> Java 2 - ISEN - 2022

---

## <i class="fa fa-users"></i> What we will see together

Generics and collections

> How can I process my data

IO and streams

> How can I load/save my data

**Unit tests**

> How can I test my code

JDBC

> How can I load/save relations between my data

JavaFX

> How can I have a fancy user interface

What’s new in java 12+

---

class: middle,center

<i class="nf nf-fa-stack_exchange" style="font-size: 7em;"></i>

# Stacktraces

---

## What are Stacktraces ?

--

In Java, runtime errors are represented as a *stack trace*:

--

```java
Exception in thread "main" java.lang.NullPointerException
	at sun.nio.fs.WindowsPath.toWindowsPath(WindowsPath.java:373)
	at sun.nio.fs.WindowsPath.resolve(WindowsPath.java:525)
	at sun.nio.fs.WindowsPath.resolve(WindowsPath.java:44)
	at isen.java2.nio.sorter.FileSorter.<init>(FileSorter.java:23)
	at isen.java2.nio.app.Application.main(Application.java:13)
```

--

It is, as the name implies, the list of all the methods involved in the current stack call.

--

It is read from top to bottom, the initial call throwing the Exception being the bottom one

---

## Reading Stacktraces

--

Reading a stack trace is a *vital* skill for a Java developer. Let's get back to our example

--

```java
Exception in thread "main" java.lang.NullPointerException
	at sun.nio.fs.WindowsPath.toWindowsPath(WindowsPath.java:373)
	at sun.nio.fs.WindowsPath.resolve(WindowsPath.java:525)
	at sun.nio.fs.WindowsPath.resolve(WindowsPath.java:44)
	at isen.java2.nio.sorter.FileSorter.<init>(FileSorter.java:23)
*	at isen.java2.nio.app.Application.main(Application.java:13)
```

--

A stack trace lists:

--

* the `exception` that has been raised in your code (`NullPointerException`)

--

* from which method it has been thrown (`Application.main`)

--

* through which method it has traversed, up to the main thread.

--

For each method, it is possible to see at which line in the code it has been thrown (`FileSorter.java:23`).

--

Depending on your *IDE*, these line numbers can be clickable so that you can get to the buggy code directly.

---

## Exception chain

--

Several exceptions can be chained together (*we will see why in the next course ;)* ), e.g.:

--

```java
isen.java2.library.exceptions.ItemAlreadyBorrowedException: Item already borrowed !
	at isen.java2.library.Library.borrowItem(Library.java:121)
	at isen.java2.library.Library.borrowBook(Library.java:100)
	at isen.java2.library.Application.main(Application.java:91)
Caused by: java.lang.NullPointerException
	at isen.java2.library.Library.borrowItem(Library.java:120)
	... 2 more
```
--

The `Caused by` indicates the originating exception. Here it explains that the `ItemAlreadyBorrowedException` has been thrown in a catch block that caught a `NullPointerException`.

--

A stack trace can have multiple `Caused by` lines.

--

The lowest of these lines is the *root* `exception`. It is the one that indicates the buggy code, and the one you have to solve.

---

## Solving exception problems

--

When reading a stack trace, the following process can be done:

--

* If there are more than one stack trace, look at the first one;

--

* Find the root cause of the stack trace;

--

* Read its message and try to understand it;

--

* Find in the first *method* listed below the root cause that is in a *class* that you write;

--

* Go to the line in your code to fix the problem!

---

class: middle,center

<i class="fas fa-question" style="font-size: 7em;"></i>

# JUnit

---

## JUnit

--

*JUnit* is a framework to add *unit tests* to a java project.

--

The goal is to be able to test the different methods of a java project.

--

*JUnit* is a *library*. The different classes used are not in the JDK.

--

To have access to the *JUnit* functionalities, it is necessary to add a *library* to our java project (*we will see later on how to do it properly*).

--

We will use *JUnit* in the 4.12 version.

---

## Interest

--

It is difficult to test all the code of a project through its normal behavior.

--

* It is slow.

--

* It is not automated.

--

* Some behaviors are difficult to test (error case).

--

* It pollutes the code with unwanted test code

---

## Test cases

--

To write our *tests*, we will create *test cases*.

--

A *test case* is a simple java class that will regroup *tests* on a functionality of a project.

--

A *test case* is composed of several *tests* most of the time.

--

```java
public randomTestCase {

   // test 1

   // test 2

}
```

---

## Test

--

A *test* is a method written in a *test case*.

--

A *test* will check that a method will return the expected result or will do the expected process when called with specific parameters.

--

A *test* does a **unique** check...

--

... or it is not a *unit test*

--

*JUnit* identify the *test* methods with the `@Test` annotation.

--

A *test* method must be public and return `void`.

--

```java
@Test
public void shouldDivideAndReturnResult() throws Exception {

// (…)

}
```

---

## Example : some code to test

```java
public class Mathematics {

   public DivisionResult divide(int dividend, int divisor) throws DivisionByZeroException {
      if(divisor == 0) {
         throw new DivisionByZeroException();
      }
      DivisionResult result = new DivisionResult();
*     result.setQuotient(dividend/divisor);
*     result.setRemainder(dividend % divisor);
      return result;
   }

}
```

---

## Example : corresponding test

```java
public class MathematicsTestCase {

*  @Test
   public void shouldDivideAndReturnResult() throws Exception {

*     // GIVEN
      Mathematics mathematics = new Mathematics();
      int dividend = 17;
      int divisor = 5;

*     // WHEN
      DivisionResult result = mathematics.divide(dividend, divisor);

*     // THEN
*     assertThat(result.getQuotient()).isEqualTo(3);
*     assertThat(result.getRemainder()).isEqualTo(2);
   }

}

```

---

## 3 steps of a test

--

The **Given** step : creation of the object (parameters for example) used for the *test*.

--

The **When** step : execution of the method we want to test.

--

The **Then** step : checks that the tested method has done everything as expected.

---

## Naming convention/habit

--

Your test will be more readable if the name of your test method is explicit

--

If the name of your method starts with "should", it is easier to have an explicit name that explains what is expected

--

`shouldReturnTrue()`, `shouldThrowNullPointer()`, `shouldGetData()`, …

---

## Assertions

--

The last *step* of a *test* is to check that the tested method has had the expected behavior.

--

To do that, *assertions* are used.

--

Multiple *assertions* will be done in a test. Remember, though, that these multiple *assertions* must test a one and unique *behavior*, otherwise it's not a unit test.

---

## Assert class

--

*JUnit* has an `Assert` class that can do *assertions*.

--

`Assert` has multiple *static methods* to do different types of *assertions*.

--

The main method is `assertEquals()`:

```java
Assert.assertEquals(3, mathematics.add(1, 2));
```

--

The `Assert` class has other static methods that allow different types of assertions :

--

```java
Assert.assertNotEquals(expected, actual);
Assert.assertNull(object);
Assert.assertNotNull(object);
Assert.assertTrue(condition);
Assert.assertFalse(condition);
```

---

## Result of a test

--

A *test* can have three different results :

--

* **Success**: the *test* has gone to its end and all the *assertions* are correct.

--

* **Failure**: at one point in the execution of the *test*, an *assertion* has been incorrect.

--

* **Error**: at one point in the execution of the *test*, an unexpected exception has been thrown.

---

## Error of a method

--

A method must be tested in every way possible and several tests will be written to test the different behaviors of one method.

--

Error behavior and exception thrown are **as important** to test as normal behavior.

--

`@Test` can have an attribute called **expected** to check for Exception :

--

```java
*@Test(expected = DivisionByZeroException.class)
public void shouldNotDivideAndThrowDivideByZeroException() throws Exception {

   // GIVEN
   Mathematics mathematics = new Mathematics();
   int dividend = 17;
   int divisor = 0;

   // WHEN
   mathematics.divide(dividend, divisor);

}
```

---

class: center,middle

.image60[![wait](images/wait.png)]

---

## Fail assertion

--

The `Assert` class has a `fail()` method that can be used to fail a test.

--

It can be used to verify that an expected exception has not been thrown :

--

```java
@Test(expected = DivisionByZeroException.class)
public void shouldNotDivideAndThrowDivideByZeroException() throws Exception {

   // GIVEN
   Mathematics mathematics = new Mathematics();
   int dividend = 17;
   int divisor = 0;

   // WHEN
   mathematics.divide(dividend, divisor);

   // THEN
*  fail("an exception is expected");

}
```

---

## Testing Exceptions

--

Sometimes you want to test more than the type of exception thrown (e.g. log message)

--

`@Test(expected...)` does not allow this. You have to use try – catch structure :

--

```java
@Test
public void shouldNotDivideAndThrowDivideByZeroException() {

   // GIVEN
   Mathematics mathematics = new Mathematics();

   // WHEN
   try {
      mathematics.divide(17, 0);
*     fail("exception is expected"); // This line should not be reached
   }

   // THEN
   catch(Exception e) {
*     assertThat(e).isInstanceOf(DivisionByZeroException.class);
*     assertThat(e.getMessage()).isNotNull();
   }

}
```

---

## @Before annotation

--

In a *test case*, methods can be annotated with the `@Before` annotation.

--

These methods will be executed before **each** test of the *test case*

--

The main objective of `@Before` is to allow a reinitialization of the environment.

--

For example, it can be:

--

* The reinitialization of the content of a directory in the file system to test a method of file manipulation.

--

* The reinitialization of the content of the database to test a method of DB access.

---

## Other annotations

--

*JUnit* has other annotations that are less useful but exist:

--

* `@After`: similar to `@Before` but executed after **each** test of a *test case*.

--

* `@BeforeClass`: execute a *static* method **once** before all the tests of a *test case*.

--

* `@AfterClass`: similar to `@BeforeClass` but executed **once** after all the tests of a *test case*

---
class: center, middle
<i class="nf nf-fa-exclamation" style="font-size: 7em;"></i>

# AssertJ

#### Better and more powerful assertions

---

## More readable assertions

--

AssertJ propose severable methods to write *assertions*.

--

The goal is to write *assertions* that are easier to read and almost make a "real sentence"

--

* Example with *JUnit*

```java
assertEquals(3, result.getQuotient())
```

--

* Example with *AssertJ*

```java
assertThat(result.getQuotient()).isEqualTo(3);
```

---

## Methods of AssertJ

--

A lot of methods are accessible to express the assertions the developer want to do.

--

The methods accessible depends on the type of the value tested :

--

```java
assertThat(booleanValue).isTrue();

assertThat(booleanValue).isFalse();

assertThat(objectValue).isNull();

assertThat(objectValue).isNotNull();

assertThat(objectValue).

isOfAnyClassIn(Integer.class, Long.class);
```

--

The best way to select the *assertion*’s methods is to use the auto-completion feature of your IDE. (`ctrl + space`)

---

## Static Imports

One more trick of Java

--

If a class has static methods, these methods can be imported directly in other classes:

--

```java
import static org.assertj.core.api.Assertions.assertThat;
```

---

## Example of use

```java
package org.acme;

*import static org.assertj.core.api.Assertions.assertThat; // static import
import org.JUnit.Test;
(...)

public class MathematicsTestCase {

   @Test
   public void shouldDivideAndReturnResult() throws Exception {

      // GIVEN
      // WHEN
      (...)

      // THEN
*     assertThat(result.getQuotient()).isEqualTo(3);
*     assertThat(result.getRemainder()).isEqualTo(2);

   }

}

```

---

class: middle,center

<i class="nf nf-fa-play" style="font-size: 7em;"></i>

# How to launch tests

---

## Test launching

--

Tests are like main java classes. They can be executed from your IDE

--

Unlike main classes, Tests do not require a main `method`

--

To launch a Test in Eclipse , simply select the test class, right click and select `Run As... --> JUnit test` , or use the `alt+shift+x,t` shorcut.

--

> Tests are not executed when you launch the `main` method of a regular class, so mind your development lifecycle !

---

## Tests report

--

As we have seen earlier, Test can have three states : *success*, *failure* or *error*.

--

JUnit will output a report containing the result and the detail for each `test` of the test case

--

* Success: nothing except a green indicator

--

* Failure:  an AssertionError describing what's went wrong, e.g.:

```java
java.lang.AssertionError: 
Expecting: <["Inclusion LineInclusion Line 2"]> to contain only: <["Inclusion Line"]>
(...)
	at isen.java2.pagebuilder.builder.PageBuilderTestCase.shouldWriteContentInWriter(PageBuilderTestCase.java:77)
```

--

* Error: a throwable indicating what's went wrong, e.g.:

```java
java.lang.StringIndexOutOfBoundsException: begin 2, end 19, length 17
	at java.base/java.lang.String.checkBoundsBeginEnd(String.java:3720)
```

---
class: middle,center

<i class="nf nf-fa-thumbs_o_up" style="font-size: 7em;"></i>


# How to write good tests

---

## When do I write my test ?

--

You test your code:

--

* While you *write* it, to guide the *design*

--

   * For **design** purposes

--

* While you *refactor* it, to ensure that you don’t introduce any *regression*

--

   * For **quality** purposes

---

## Design purposes

--

Test should appear in your code as soon as you try to implement some functionalities

--

Tests should be sufficient to drive you through the implementation

--

You want as many test as necessary to write a functionality, **BUT NO MORE**

---
class: center,middle
## Test Driven Development

.image90[![TDD](images/tdd.png)]

---

## Test Driven Development

--

First, write a test...

--

Does it fail ?

--

* *No*: write another test...

--

* *Yes*: write as *few* code as needed to make it pass

--

Once the test goes green, *refactor* your code

--

Start again...

--

> Compilation errors are failed tests

---

## Some words about quality

--

Tests are part of your codebase

--

* Maintenance cost

--

* Importance of good design for tests, too

--

The more you test, the more you add to maintenance costs

--

For quality purposes, write as many tests as needed to ensure *balance* between risk mitigation and maintenance cost

--

> Golden rule: when you start, it's more than you think...

---

## Some other tools beyond *JUnit*

--

Mocks

--

* Replace implementation that you don’t have by expected behaviors

--

* Mockito framework

--

Golden master

--

* Compare the output of a program to a reference file

---

## Golden rules

--

Write test *before* code

--

Test only *one thing* at a time

--

Each test must be *self sufficient*

--

Do not assume state from a previous test

--

> Otherwise your test will randomly fail, and is useless

--

Do not test your runtime dependencies (e.g. external BDD)

--

> Otherwise it’s not unit test anymore

--

Do not test your external libraries (e.g. JDBC access layer)

--

> Otherwise you are testing someone else’s work

---
class: middle, center
<i class="nf nf-mdi-package_variant_closed" style="font-size: 7em;"></i>

# Maven

---

## What is maven ?

--

Maven is a tool to facilitate the build of Java projects.

--

It is meant to describe the lifecycle of your application (*descriptive* paradigm, as opposed to `cmake` or `ant`).

--

Maven is a very powerful and complex tool.

--

This is just an introduction, a "taste" of what can be done with it

---

## Build lifecycle

--

Maven create a process to build a Java application from a Java *project*.

--

This process consists of several steps :

--

* Generate configuration

--

* Download dependencies

--

* Compile source code

--

* Compile and execute tests

--

* Create an archive (`.jar` or `.war`)

--

* Generate documentation

--

* Publish the packaged application

--

* …

---

## Version Numbers

--

A maven project has a version number.

--

Development versions are called « `snapshots` »

--

For each version, a `release` is done to transform a `snapshot` into an *immutable* version, called « `release` »,

--

Dependencies of a `release` must be `releases`, too

---

## Project Object Model

--

All the configuration of a maven *project* is done in a file called `pom.xml`.

--

It is a XML file that will describe the Java *project*.

--

Maven rely heavily on the "*convention over configuration*" principle.

--

Information in the `pom.xml` will overwrite the default convention

---

## Project Object Model

```xml
<project xmlns=*"http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XML*
*Schema-instance"* xsi:schemaLocation=*"http://maven.apache.org/POM/4.0.0*
*http://maven.apache.org/xsd/maven-4.0.0.xsd">*
   <modelVersion>4.0.0</modelVersion>
   <groupId>isen.java2</groupId>
   <artifactId>course3-example</artifactId>
   <version>0.0.1-SNAPSHOT</version>
   <properties>
      <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
      <maven.compiler.source>1.8</maven.compiler.source>
      <maven.compiler.target>1.8</maven.compiler.target>
   </properties>
   <dependencies>
      <dependency>
*        <groupId>junit</groupId>
*        <artifactId>junit</artifactId>
*        <version>4.12</version>
      </dependency>
   </dependencies>
</project>
```

---

## Basic information

--

Some pieces of information are meant to identify the *project* :

--

* `groupId` identifies the *project* across all the maven projects in the world.

--

  * `isen.java2`

--

* `artifactId` is the name of the java *project*.

--

   * `course3-example`

--

* `version` is the version of the *project*

--

   * `0.0.1-SNAPSHOT`

---

## Default organization

--

A maven *project* is organized in a specific way. Each part of the *project* has to be in the correct place

--

Directory|Purpose
---|---
`./` (root) directory|Contains the pom file and the following directories
`./src/main/java`|Source code of the *project*
`./src/main/resources`|Resources like properties files (non Java code)
`./src/test/java`|Testing source code of the *project*
`./src/test/resources`|Testing specific resources (non Java code)
`./target`|Compiled and packaged *project*

---

## Dependency management

--

A central feature of Maven is to manage the *dependencies* of a java *project*.

```xml
<dependencies>
   <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
      <scope>test</scope>
   </dependency>
```

--

Every library needed is specified in the `pom.xml` file.

--

```xml
   <dependency>
      <groupId>org.assertj</groupId>
      <artifactId>assertj-core</artifactId>
      <version>3.2.0</version>
      <scope>test</scope>
   </dependency>
</dependencies>
```

---

## Dependency management

--

Each of the *dependencies* is a maven *project* of its own.

--

It is described with the same XML tags than the maven *project* we create :

--

* `groupId`

--

* `artefactId`

--

* `version`

---

## Scope

--

Each *dependency* is associated with a *scope*.

--

It will determine when the content of the *dependency* can be used.

--

If not specified, the default *scope* is `compile`, the *dependency* is needed all the time.

---

## Scope

--

The `runtime` *scope* means that the class of the *dependencies* can be used only when running the application but not when compiling the code.

--

The `provided` scope is the opposite, it can be used when compiling the code but is not needed when running the application

--

The `test` *scope* can only be used by classes in the `src/test/java` directory.

--

This scope is the one to use for test *dependencies* like *JUnit* or AssertJ

---

## Getting dependencies

--

When building a *project*, Maven will import the needed *dependencies*.

--

To import a *dependency*, it will be downloaded from a distant *repository*, most often `Maven Central`.

--

Then, it is kept in a local *repository* on the developer computer, located in the `~/.m2` folder

---

## Getting dependencies

<div class="mermaid center">
graph TB
    subgraph TestProject
    id["pom.xml <br/> junit.junit:4.12 <br/> org.assertj.assertj-core:3.2.0"]
    end
</div>

---

## Getting dependencies

<section class="center">
  <video width="720" height="480" controls autoplay>
    <source src="videos/maven.mkv" type="video/webm" />
  </video>
</section>
---

## Practical problems

--

Since Maven downloads *dependencies* from the internet, proxies can be a problem.

--

Maven can work with proxies with additional configuration, through a file called `setttings.xml` locate in the local repository (`~/.m2`)

---

class: center, middle
<i class="fas fa-play" style="font-size: 7em;"></i>

# now let's have some fun

### with [practical work #4](../pw04/assignment/Java2_PW04_Tests/): Soooo many errors

### and [Bonus stage](../pw04/assignment/Java2_PW04_Tests/#bonus-stage): "Gilded Rose" Kata

---

## Sources

--

<http://joel-costigliola.github.io/assertj/>

--

<https://maven.apache.org/users/>

       </textarea>
   <script src="https://cdn.jsdelivr.net/npm/mermaid@8.8.4/dist/mermaid.min.js"></script>
   <script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
   <script src="js/slideshow.js"></script>
   <!-- <script src="js/diag.js"></script> -->
   <script>
   mermaid.initialize({
    startOnLoad: false,
    cloneCssStyles: false,
    theme: 'dark',
   });
   function initMermaid(s) {
      var diagrams = document.querySelectorAll(".mermaid");
      var i;
      for (i = 0; i < diagrams.length; i++) {
         if (diagrams[i].offsetWidth > 0) {
         mermaid.init(undefined, diagrams[i]);
         }
      }
   }
   slideshow.on("afterShowSlide", initMermaid);
   initMermaid(slideshow.getSlides()[slideshow.getCurrentSlideIndex()]);
  </script>

</body>

</html>
