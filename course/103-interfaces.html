<!DOCTYPE html>
<html>

<head profile="http://www.w3.org/2005/10/profile">
    <link rel="icon" 
          type="image/png" 
          href="images/java.png">
    <title>103: Interfaces</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="https://kit.fontawesome.com/79b3831d6d.js" crossorigin="anonymous"></script>
</head>

<body>
    <textarea id="source">

name: inverse
class: center, middle
<i class="fas fa-sitemap" style="font-size: 7em;"></i>

# 103: Interfaces

#### <i class="fab fa-java"></i> Java 2 - ISEN - 2021

---

## Interface

--

An *interface* can be created using the keyword `interface`. It is created like a class.

--

```java
public interface Talkable {

(…)

}
```

--

An interface can’t be instantiated.

--

```java
Talkable talkableBeing = `new Talkable()`; // Won't compile
```

---

## Content of an interface

--

An interface is composed of a list of methods.

--

By default, these methods have no implementation *(more on that later)*.

--

```java
public interface Talkable {

String talk(String speech);

String getLanguage();

}
```

---

## Inheritance

--

Inheritance of interfaces is possible. It is similar to inheritance of classes.

--

### Bilingual interface

```java
public interface Bilingual extends Talkable {

String talk(String speech, String lang);

String getDefaultLanguage();

}
```

--

### List of available methods

```java
String talk(String speech);              // From Talkable

String getLanguage();                    // From Talkable

String talk(String speech, String lang); // From Bilingual

String getDefaultLanguage();             // From Bilingual
```

---

## Multiple inheritance

--

An interface can extend several other interfaces.

--

### Bilingual interface

```java
public interface Bilingual extends Talkable, MultiLanguageSupport {

(…)

}
```

--

### List of available methods

```java
String talk(String speech);              // From Talkable

String getLanguage();                    // From Talkable

String talk(String speech, String lang); // From Bilingual

String getDefaultLanguage();             // From Bilingual

String[] getSupportedLanguages();        // From MultiLanguageSupport
```

---

## Implementing an interface

--

A class can implement an interface with the keyword `implements`.

--

```java
public class Human implements Talkable {

(…)

}
```

--

It’s possible to implement an interface and have a parent class.

--

```java
public class Human extends Species implements Talkable {

(…)

}
```

---

## Implementation of the methods

--

A class that implements an interface **must** implement all the methods described in the interface.

--

Except if the class is abstract.

--

An interface is a contract. The class must respect this contract.

--

```java
public class Human implements Talkable {

    private String language;

    @Override
    public void talk(String speech) {
        System.out.println(speech);
    }

    @Override
    public String getLanguage() {
        return language;
    }

}
```

---

## Multiple implementation

--

A class can implement several interfaces. It is the only way in Java to have something similar to multiple inheritance.

--

```java
public class Human extends Species implements Talkable, Comparable<Human> {

(…)

}
```

---

## Polymorphism

--

A class that implements an interface can be typed as the interface.

--

```java
Talkable human = new Human();

Talkable parrot = new Parrot();
```

--

Calling a method on an object typed as an interface will call the implementation of the object’s class.

--

```java
Talkable human = new Human();

Talkable parrot = new Parrot();

human.talk("hello, world!");  // Console output: hello, world!

parrot.talk("hello, world!"); // Console output: Qwak hello, world! qwak
```

---
exclude: true
class: middle, center
## Abstract class vs Interface

**Abstract class**|**Interface**
---|---
|delegate implementation
Can have attributes|Multiple interfaces can be implemented by one class
Can play with visibility|Everything is public
Only one ancestor allowed|

---
## Abstract class vs Interface

.cols[
.fifty[
### Abstract Class

* Provides default implementation
* Can have attributes
* Can play with visibility
* Only one ancestor allowed
]

.fifty[

### Interface

* Delegates implementation (but you can provide a default one)
* Cannot have attributes
* Multiple interfaces can be implemented by one class
* Everything is public
]
]

--

Abstract classes bring the concept of **inheritance** whereas interfaces bring the concept of **composition**.

--

In terms of coupling, **inheritance** is stronger than **composition**.

--

Separate concerns :

* use **inheritance** for **specialization,**

* use **composition** for aggregation of behaviors.

---

## API evolution

What if I want to add a *new* method to my interface ?

```java
public interface Talkable {
    String talk(String speech);
    String getLanguage();
*    String sayHi();
}

* public class Human implements Talkable {  // won't compile
    private String language;
    @Override
    public void talk(String speech) {
        System.out.println(speech);
    }
    @Override
    public String getLanguage() {
        return language;
    }
}
```

---

class: center, middle

.image90[![Here comes a  new challenger](images/challenger.png)]

---

## default methods in Interfaces

Java 8 introduces the modifier `default` which can be used to declare a default implementation in an Interface

--

```java
public interface Talkable {
    String talk(String speech);
    String getLanguage();

*    default String sayHi() {
*        System.out.println("Hi");
*    }
}
```

--

A default method is implicitly public.

--

A default method *must* provide an implementation.

--

This default implementation wil be available to all classes implementing the interface:

--

```java
var human = new Human();
human.sayHi();          // returns "Hi"
```

???

Quoting Wikipedia: "Default methods allow an author of an API to add new methods to an interface without breaking the old code using it. Although it was not their primary intent,default methods also allow multiple inheritance of behavior (but not state)."

---

## Limitations of the default methods

--

When implementing several interfaces providing the same default method, the implementing class *must* override the default method.

--

```java
public interface Talkable {
(...)
    default String sayHi() {
        System.out.println("Hi");
    }
}

public interface Bilingual {
(...)
    default String sayHi() {
        System.out.println("Hi");
    }
}

public class GermanParrot implements Talkable, Bilingual {
    @override
    public String sayHi() {
        System.out.println("Güten Tag");
    }
}
```

---

## Static methods in Interfaces

--

When declaring utility methods, it is convenient to declare static methods, to avoid having to create an object.

--

Up until Java 7, static utility methods were traditionally grouped into abstract classes. This comes with limitations

* one can instantiate such an abstract class, which consumes memory for nothing
* one can extends an abstract class, adding mess to your architecture by exposing methods to inherited objects that are totally irrelevant

--

Since Java 8, it is possible to declare such methods in Interfaces, *ensuring that nobody will try to instantiate your utility class...*

--

```java
public interface Talkable {
(...)
    static String sanitizeSpeech(String speech) {
        return speech.strip(); // new in Java 11, drop leading and 
                               // trailing spaces in an UTF8-friendly fashion
    }
}

```

---

## Private methods in Interfaces

--

Now that we have methods implementations in Interfaces, it would be convenient to be able to share code, without having to expose it through a default, hence public, method...

--

```java
public interface Talkable {
(...)
    static String sanitizeLeadingSpeech(String speech) {
*        System.out.println("sanitizing speech");
        return speech.stripLeading(); // one more Java 11 candy
    }
    
    static String sanitizeTrailingSpeech(String speech) {
*        System.out.println("sanitizing speech");
        return speech.stripTrailing(); // yet another one...
    }
}

```

--

Here enters private methods in Interfaces, introduced in Java 9

--

```java
private static void log() {
    System.out.println("sanitizing speech");
}
```

???

https://openjdk.java.net/jeps/213 : "[...]enabling non abstract methods of an interface to share code between them"

---

class: center, middle
<i class="fas fa-play" style="font-size: 7em;"></i>

# now let's have some fun

### with [practical work #1](https://java210.gitlab.io/java2-2022/pw01/assignment/Java2_PW01_java_interfaces/)


        </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js">
    </script>
    <script src="js/slideshow.js">
    </script>

</body>

</html>
