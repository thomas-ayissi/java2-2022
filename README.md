# Welcome to the JAVA2 course 

This repository contains the documents that will be used during the course.

An online version of the courses and practical works is hosted on the pages associated with the repository. You will find a link a the top of this page.

For practical work that require some starting code, you will find the relevant code in the `/practical_work/pwXX/code` directory.

After each session, you will also be able to find a possible solution for the practical exercises. in the `/practical_work/pwXX/correction` directory.

## How to use this repository ?

* Have a look at the aforementioned link to the online version,
* clone this repository to have the specific starting code for practical works that require it.
