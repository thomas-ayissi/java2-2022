/**
 * 
 */
package isen.java2.library;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import isen.java2.library.exceptions.ItemAlreadyBorrowedException;
import isen.java2.library.exceptions.ItemNotFoundException;
import isen.java2.library.model.Book;
import isen.java2.library.model.CulturalItem;
import isen.java2.library.model.Film;
import isen.java2.library.model.Genre;

/**
 * This is our main library class, which will hold all the logic of
 * CulturalItems handling
 * 
 * @author Philippe Duval
 */
public class Library {

	// Private field for films handling. note that we make the choice to use a
	// list, which allows us to have duplicates. We use generics to specialize
	// the objects used in the list.
	// you could use another storage structure, as long as it is sortable, as
	// it's required for the exercise
	// Note that we use an abstract type, the implementation is up to the caller.
	private List<Film> listOfFilms;

	// Private field for books handling.
	private List<Book> listOfBooks;

	/**
	 * Simple parameterized constructor
	 * 
	 * @param listOfFilms
	 *            the list of films available in the library
	 * @param listOfBooks
	 *            the list of books available in the library
	 */
	public Library(List<Film> listOfFilms, List<Book> listOfBooks) {
		this.listOfFilms = listOfFilms;
		this.listOfBooks = listOfBooks;
	}

	/**
	 * Adds a film to the library
	 * 
	 * @param film
	 */
	public void add(Film film) {
		this.listOfFilms.add(film);
	}

	/**
	 * Adds a book to the library
	 * 
	 * @param book
	 */
	public void add(Book book) {
		this.listOfBooks.add(book);
	}

	/**
	 * Allows a given user to borrow a film
	 * 
	 * @param filmTitle
	 * @param borrower
	 * @return
	 * @throws ItemNotFoundException
	 *             if the title of the film does not match an existing item in
	 *             the library
	 * @throws ItemAlreadyBorrowedException
	 *             if the film is already borrowed
	 */
	public Film borrowFilm(String filmTitle, String borrower)
			throws ItemNotFoundException, ItemAlreadyBorrowedException {
		return borrowItem(filmTitle, borrower, this.listOfFilms);
	}

	/**
	 * 
	 * @param bookTitle
	 * @param borrower
	 * @return
	 * @throws ItemNotFoundException
	 *             if the title of the film does not match an existing item in
	 *             the library
	 * @throws ItemAlreadyBorrowedException
	 *             if the film is already borrowed
	 */
	public Book borrowBook(String bookTitle, String borrower)
			throws ItemNotFoundException, ItemAlreadyBorrowedException {
		return borrowItem(bookTitle, borrower, this.listOfBooks);
	}

	/**
	 * As the borrowBook and borrowFilm implement practically the same logic, we
	 * factorize their code in a private method that does all the logic using
	 * generics. we will use a generic T extending CulturalItem to parameterize
	 * our class. this generic will be inferred at compilation time depending on
	 * the calling method's signature
	 * 
	 * @param itemTitle
	 * @param borrower
	 * @return
	 * @throws ItemAlreadyBorrowedException
	 * @throws ItemNotFoundException
	 */
	private <T extends CulturalItem> T borrowItem(String itemTitle, String borrower, List<T> listOfItems)
			throws ItemAlreadyBorrowedException, ItemNotFoundException {
		T item = this.searchByTitle(itemTitle, listOfItems);
		if (item.isBorrowed()) {
			throw new ItemAlreadyBorrowedException();
		}else {
			item.borrow(borrower);
			return item;
		}
	}

	/**
	 * Method listing what was borrowed by a certain user
	 * 
	 * @param borrower
	 */
	public void printBorrowedItems(String borrower) {
		this.printSortedList(this.filterByBorrower(this.listOfBooks, borrower), "Borrowed Books by " + borrower);
		this.printSortedList(this.filterByBorrower(this.listOfFilms, borrower), "Borrowed Films by " + borrower);
	}

	/**
	 * Method allowing to rate a film
	 * 
	 * @param filmTitle
	 * @param person
	 * @param rating
	 * @throws ItemNotFoundException
	 */
	public void rateFilm(String filmTitle, String person, Integer rating) throws ItemNotFoundException {
		Film film = this.searchByTitle(filmTitle, this.listOfFilms);
		film.addRating(person, rating);
	}

	/**
	 * Method allowing to rate a book
	 * 
	 * @param bookTitle
	 * @param person
	 * @param rating
	 * @throws ItemNotFoundException
	 */
	public void rateBook(String bookTitle, String person, Integer rating) throws ItemNotFoundException {
		Book book = this.searchByTitle(bookTitle, this.listOfBooks);
		book.addRating(person, rating);
	}

	/**
	 * Method returning the top ten items according to all ratings in the
	 * library
	 */
	public void printTopTenItems() {
		// Create a list with all items, by aggregating the two lists.
		List<CulturalItem> top = new LinkedList<>(this.listOfBooks);
		top.addAll(this.listOfFilms);
		// sort by averageRating, using an inner comparator class
		Collections.sort(top, new Comparator<CulturalItem>() {

			@Override
			public int compare(CulturalItem o1, CulturalItem o2) {
				// as averageRatting returns a double, the implementation is
				// straightforward
				return o2.getAverageRating().compareTo(o1.getAverageRating());
			}

		});
		// print list (TRAP: you cannot use the print method, as it already
		// makes sorting for you)
		// here we use a ternary operator to extract the sublist, in order to
		// avoid a indexOutOfBoundException
		this.printList(top.size() > 10 ? top.subList(0, 10) : top, "Notre Best Of");
	}

	/**
	 * Here again, we use a private method using generics to factorize our code.
	 * Note that in the case we want to handle another type of CulturalItem, all
	 * these private generic methods stay unchanged
	 * 
	 * @param title
	 * @return
	 */
	private <T extends CulturalItem> T searchByTitle(String title, List<T> items) throws ItemNotFoundException {
		for (T item : items) {
			if (item.getTitle().equals(title)) {
				return item;
			}
		}
		// note: if we find the item in the previous loop, we will never reach
		// this code, so the exception wont be thrown.
		// if we don't find the book, we exit the loop, and throw the exception.
		// there is no need for a local variable here.
		throw new ItemNotFoundException();
	}

	/**
	 * Method printing the two lists of our library, sorted by alphabetical
	 * order
	 */
	public void printCatalogue() {
		this.printSortedList(this.listOfBooks, "Our Books");
		this.printSortedList(this.listOfFilms, "Our Films");
	}

	/**
	 * Method searching items by genre and printing the result in alphabetical
	 * order
	 * 
	 * @param genre
	 */
	public void searchByGenre(Genre genre) {
		this.printSortedList(this.filterByGenre(genre, this.listOfBooks), "Books of " + genre);
		this.printSortedList(this.filterByGenre(genre, this.listOfFilms), "Films of " + genre);

	}

	/**
	 * And yet another private method using generics, this time to filter our
	 * collections by genre
	 * 
	 * @param genre
	 * @param items
	 * @return
	 */
	private <T extends CulturalItem> List<T> filterByGenre(Genre genre, List<T> items) {

		// Here we have to use a local variable : remember that the items list
		// though passed by value, still reference our object's list. filtering
		// it directly without passing by a local variable would be permanent !
		List<T> filteredList = new LinkedList<>(items);

		// we use an inner class that is a predicate, which allows us to filter
		// the collection without having to call an iterator
		filteredList.removeIf(new Predicate<CulturalItem>() {

			@Override
			public boolean test(CulturalItem t) {
				return !t.getGenres().contains(genre);
			}
		});
		return filteredList;
	}

	/**
	 * Method allowing us to filter by borrower a list of items. Again, we use
	 * generics.
	 * 
	 * @param items
	 * @param borrower
	 * @param genre
	 * @return
	 */
	private <T extends CulturalItem> List<T> filterByBorrower(List<T> items, String borrower) {
		// again we filter a local copy of our list, using a predicate.
		List<T> filteredList = new LinkedList<>(items);

		filteredList.removeIf(new Predicate<CulturalItem>() {

			@Override
			public boolean test(CulturalItem t) {
				// filters the list by excluding the items not borrowed, or
				// borrowed by another person
				return (!t.isBorrowed()) || (t.isBorrowed() && !t.getBorrower().equals(borrower));
			}
		});
		return filteredList;
	}

	/**
	 * You bet it ! private method using generics, to print a list of items
	 * 
	 * @param items
	 * @param type
	 */
	private <T extends CulturalItem> void printSortedList(List<T> items, String type) {
		// Handy method allowing us to sort a list by it's natural order (see
		// the cultural item class for further explanation)
		Collections.sort(items);
		this.printList(items, type);
	}

	/**
	 * @param items
	 */
	private <T extends CulturalItem> void printList(List<T> items, String type) {
		System.out.println("======= " + type + " =============");
		// this way to iterate is not the shortest way to print our list, but
		// demonstrates another use of inner classes, this time with a consumer
		items.forEach(new Consumer<T>() {

			@Override
			public void accept(T it) {
				it.print();

			}
		});
	}
}
