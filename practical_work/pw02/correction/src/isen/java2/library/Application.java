/**
 * 
 */
package isen.java2.library;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import isen.java2.library.exceptions.ItemAlreadyBorrowedException;
import isen.java2.library.exceptions.ItemNotFoundException;
import isen.java2.library.model.Book;
import isen.java2.library.model.Film;
import isen.java2.library.model.Genre;

/**
 * @author Philippe Duval
 *
 */
public class Application {

	/**
	 * Private constructor to hide it and prevent this class from being instanciated
	 */
	private Application() {
	}

	/**
	 * Our main entrypoint
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Create some Books (using Arrays.asList and Collections.singleton convenience methods to keep code compact)
		Book warOfWorlds = new Book("La guerre des mondes",  new HashSet<Genre>(Arrays.asList(Genre.SCIENCE_FICTION,Genre.THRILLER)), "Aldrous Huxley");
		warOfWorlds.print();
		Book shining = new Book("Shining", new HashSet<Genre>(Collections.singleton(Genre.HORROR)),"Stephen King");
		shining.print();

		// Create some films
		Film fightClub = new Film("Fight Club",
				new HashSet<>(Collections.singleton(Genre.THRILLER)),
				"David Fincher",new HashSet<>(Arrays.asList("Brad Pitt","Edward Norton","Edward Norton")));
		fightClub.print();
		Film evilDead = new Film("Evil Dead",new HashSet<>(Collections.singleton(Genre.HORROR)),
				"Sam Raimi",new HashSet<>(Arrays.asList("Bruce Campbell","Ellen Sandweiss")));
		evilDead.print();
		Film eventHorizon = new Film("Event Horizon",new HashSet<>(Arrays.asList(Genre.HORROR,Genre.SCIENCE_FICTION)),
				"Paul W. S. Anderson",new HashSet<>(Arrays.asList("Laurence Fishburne","Sam Neill ")));
		eventHorizon.print();
		
		// Create a library, and have fun with it
		List<Film> listOfFilms = new LinkedList<>(Arrays.asList(evilDead,fightClub,eventHorizon));
		List<Book> listOfBook = new LinkedList<>(Arrays.asList(warOfWorlds,shining));
		Library myLibrary = new Library(listOfFilms,listOfBook);
		// Let's add some more items to make use of our newly created methods
		myLibrary.add(new Book("Dune",Collections.singleton(Genre.SCIENCE_FICTION),"Frank Herbert"));
		myLibrary.add(new Book("The Lord of the rings",Collections.singleton(Genre.FANTASY),"JRR Tolkien"));
		myLibrary.add(new Book("A game of thrones",Collections.singleton(Genre.FANTASY),"Georges RR Martin"));
		myLibrary.add(new Book("It",Collections.singleton(Genre.HORROR),"Stephen King"));
		myLibrary.add(new Book("Alice's adventures in wonderland",Collections.singleton(Genre.FANTASY),"Lewis Carroll"));
		
		myLibrary.add(new Film("The good, the bad and the ugly", Collections.singleton(Genre.WESTERN), "Sergio Leone",
                new HashSet<>(Arrays.asList("Clint Eastwood", "Lee Van Cleef"))));
		myLibrary.add(new Film("The Godfather", new HashSet<>(Arrays.asList(Genre.DRAMA, Genre.THRILLER)), "Francis Ford Coppola",
                new HashSet<>(Arrays.asList("Clint Eastwood", "Lee Van Cleef"))));
		
		System.out.println("Our catalogue contains :");
		myLibrary.printCatalogue();
		System.out.println("Our SF items are :");
		myLibrary.searchByGenre(Genre.SCIENCE_FICTION);
		System.out.println("Our catalogue still contains :");
		myLibrary.printCatalogue();
		System.out.println("Or Horror items are :");
		myLibrary.searchByGenre(Genre.HORROR);
		
		// Let's borrow some items and see how it goes
		try {
			Film randomFilm = myLibrary.borrowFilm("Fight Club", "Philippe");
			Book myShining = myLibrary.borrowBook("Shining", "Philippe");
			System.out.println("We borrowed "+myShining + " and "+randomFilm);
		} catch (ItemNotFoundException | ItemAlreadyBorrowedException e) {
			// This should be good
			e.printStackTrace();
		}
		
		try {
			@SuppressWarnings("unused") //an example of what we saw in the class about compilator warnings suppression
			Book anotherShining = myLibrary.borrowBook("Shining", "Martin");
		} catch (ItemAlreadyBorrowedException e) {
			System.out.println("Oups, item already borrowed");
			e.printStackTrace();
		} catch (ItemNotFoundException e) {
			// We should not go there
			e.printStackTrace();
		}

		try {
			@SuppressWarnings("unused")
			Book unknownBook = myLibrary.borrowBook("Les fleurs du mal", "Philippe");
		} catch (ItemNotFoundException e) {
			System.out.println("Oups, this book isn't available at the library");
			e.printStackTrace();
		} catch (ItemAlreadyBorrowedException e) {
			// We should not go there
			e.printStackTrace();
		}
		
		//what did we borrow ?
		System.out.println("user Philippe borrowed:");
		myLibrary.printBorrowedItems("Philippe");
		System.out.println("State of the library:");
		myLibrary.printCatalogue();
		
		// Let's rate some items now, and see how it goes
		try {
			myLibrary.rateBook("Shining", "James", 2);
			myLibrary.rateFilm("Evil Dead","Karl",5);
			myLibrary.rateFilm("Fight Club", "Luc", 4);
		} catch (ItemNotFoundException e) {
			// Should not occur
			e.printStackTrace();
		}
		
		// And now, what are our top items ?
		myLibrary.printTopTenItems();
		
	}

}
