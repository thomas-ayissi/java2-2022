# Practical Work 02: Managing a library

## Intro

-----

In this exercise, you will create your own Library Manager using what you learn about *generics* and *collections*.

## Instructions

-----

### Creation of the project

Inside your favorite *IDE*, create a new *Java* Project.

!!! warning
    The minimum required version of *Java* is JDK 1.11

All your *classes* should be in the ``isen.java2.library package``. You will create sub *packages* when needed to keep your project nicely organized.

Create a ``Library`` *class* that will represent our library, it will be the main *class* of our project.

Create an ``Application`` *class* with a ``main`` *method*. You will use this *class* to test your work throughout the creation of your project. You can start by instantiating a ``Library`` *object*.

### items

At the start of all good libraries are items that people wants to borrow. Your library can contain books and films.

Create an *enum class* called ``Genre``. It could have the following values:
*COMEDY, DRAMA, HORROR, SCIENCE_FICTION, THRILLER* or any genre you want.

Then, create an *abstract class* ``CulturalItem``. It will have the following attributes:

- A ``String`` called ``title``
- A *collection* of ``Genre`` called ``genres``

The ``title`` *attribute* is self-explanatory. The ``genres`` *attribute* should be a ``Collection`` *implementation* of all the ``genre`` of the item. Choose the correct type of ``Collection``: we can't have the same ``genre`` twice.

Add *getters*, *setters* and an *abstract method* ``print``:

```java
public abstract void print();
```

!!! hint
    This can be done easily in Eclipse using the `Alt`+`Shift`+`S` submenu

Create two *classes* that extend ``CulturalItems``: ``Book`` and ``Film``.

|                       | Book *class*            | Film *class*            |
:---:|---|---
| **Additional attributes** | ``author`` (a ``String``)     | <ul><li>``director`` (a ``String``),</li><li>``actors`` (a ``set`` of ``String``)</li></ul>    |
| **Print *method* result**   | "Title" written by author    | "Title" directed by director with actor1, actor2, actor3  |

Add all *constructors*, *getters* and *setters* necessary. Test your code by creating and printing some ``books`` and ``films``.

!!! hint
    The ``Arrays`` and ``Collections`` utility *classes* can help you keeping your instantiations tidy. See the course for more info.

### Storing and printing items

In the ``Library`` *class*, add a ``list`` of ``films`` and a ``list`` of ``books``. In the *constructor* of the *class*, instantiate both ``list``.

Create the following *methods*:

Signature|Description
---|---
`#!java public void add(Film film)`        | Add a ``film`` in the library
`#!java public void add(Book book)`        | Add a ``book`` in the library
`#!java public void printCatalogue()`     | Print all ``books`` and ``films`` of the library in the standard output. Both lists should be sorted by ``title``.
`#!java public void searchByGenre(Genre genre)`  | Similar to the ``printCatalogue`` *method* but print only ``items`` with the correct ``genre``.

You should try to factorize your code. For instance, a *private method*
that prints all the content of a ``list`` of ``items`` will be useful.

!!! hint
    Generics will come handy if you want to factorize your code.

Test your *methods* by adding ``books`` and ``films`` to your library and printing its content.

### Borrowing items

You will now add the functionality to borrow ``items`` from the library.
Start by adding the following *attributes* to the ``CulturalItem`` *class* with their associated *getter* *method*:

- A ``boolean`` called ``borrowed``
- A `String` called ``borrower``

Create the following methods:

  Signature |                            Description
  ---|---
  `#!java public void borrow(String borrower)`|   Set the ``borrower`` and mark the ``item`` as ``borrowed``
  `#!java public void returnBack()`|              Unmark the ``item`` as ``borrowed`` and delete the ``borrower``

Change the ``print`` *method* to show if the item is borrowed (B) or available
(A).

Now in the ``Library`` *class*, add the following *methods*:

|Signature|
|---|
|`#!java public Film borrowFilm(String filmTitle, String borrower) throws ItemNotFoundException, ItemAlreadyBorrowedException`|
|`#!java public Film borrowFilm(String filmTitle, String borrower) throws ItemNotFoundException, ItemAlreadyBorrowedException`|
|`#!java public Book borrowBook(String bookTitle, String borrower) throws ItemNotFoundException, ItemAlreadyBorrowedException`|
|`#!java public void printBorrowedItems(String borrower)`|

You will need to create the 2 `exceptions`.

These two `borrow` *methods* have the same implementation:

- Browse the library for the appropriate items : same type (book or film) and title
- If there is no item, throw the `ItemNotFoundException`
- If there are one or more items:
    - If all are borrowed : throw the `ItemAlreadyBorrowedException`
    - Or else borrow one of the item and returns it

### Ratings

The last functionality of your library is to add ratings to items. Add a
`ratings` *attribute* in the `CulturalItem` *class* that is a `Map` with a person name (`String`) as key and a rating (`Integer`) as value. Add a *getter* method.

Instantiate the `Map` in the constructor and add the following *methods* to
the `CulturalItem` class:

Signature|Description
---|---
`#!java public void addRating(String person, Integer rating)`|Adds a rating to the item
`#!java public Double getAverageRating()`|Returns the average rating

Change the `print()` *method* to add the average rating of the item.

In the `Library` *class*, add the following *methods*:

|Signature|
|---|
|`#!java public void rateFilm(String filmTitle, String person, Integer rating) throws ItemNotFoundException`|
|`#!java public void rateBook(String bookTitle, String person, Integer rating) throws ItemNotFoundException`|
|`#!java public void printTopTenItems()`|

The `printTopTenItems` *method* prints in the standard output the items with
the best average rating regardless of the type (book or film).
