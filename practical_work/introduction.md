# Introduction

Here you will find all the assignments for the practical works. All the documents follow the same convention :

- Java or programming *concepts* like *classes*, *methods*, and *attributes*, will be rendered in *italics*.
- Java ``Objects`` and ``types`` will be rendered in ``code blocs``
- ``#!java public final java_code()`` will be rendered with highlighting  
- ``keystrokes`` will also be rendered as code blocs, e.g. ``Alt``+``F4`` or ``Alt``+``Shift``+``X``,``T``.

> Tips will be rendered as quotes

!!! warning
    Important indication will be rendered as admonition

Code blocs are copy-pasteable by clicking the icon on the upper right side of the block. It will save you some time and typos. E.g.

```java
public static void main(){

}
```

To begin this course, you should clone this repository using the ``git`` tool that you installed in the [prerequisites]. To do this, use the following command in a terminal:

```shell
git clone <repo>
```

For each assignment, you will find all the tasks in the text, so read along carefully. They are not pointed out, so be sure that you go through all the text. *I wrote it for you to read!*

For relevant assignments, you will also find startup code. this will be indicated in the assignment, and will not be the case everytime.it will be available under the ``code`` repertory of each relevant assignment

Finally, at the end of each session, I'll commit a correction of the assignment on the repository. This won't be a definitive answer, but my take at the problem, showing you best practices and what I consider clean code. It will be available under the ``correction```repository of each assignment.
