# Practical Work 01: some basic java & interfaces

## Intro

-----

In this first work, you are going to write some basic *java*. Do not forget to use all the available tooling of your *IDE*! Your development will be much faster!

## Instructions

-----

### Creation of the project

Inside your favorite *IDE*, create a new *Java* Project.

!!! warning  
    The minimum required version of *Java* is **``JDK 11``**

### Creation of the packages

You have to create the following *packages* in your project:

- ``isen.java2.interfaces.app``
- ``isen.java2.interfaces.species``
- ``isen.java2.interfaces.species.impl``
- ``isen.java2.interfaces.bodypart``
- ``isen.java2.interfaces.exception``
- ``isen.java2.interfaces.factory``
- ``isen.java2.interfaces.factory.impl``

!!! hint
    To quickly create packages in Eclipse, use `Alt`+`Shift`+`N`,`P`

Create an ``Application`` *class* in the ``isen.java2.interfaces.app`` *package* with a main *method*. You
will use this *class* to test your work throughout the creation of your
project.

### The `exceptions`

Let's create some *classes* in the ``isen.java2.interfaces.exception`` *package*.

!!! hint
    To quickly create classes in Eclipse, use `Alt`+`Shift`+`N`,`C`

Class|Parent
:---:|:---:
``BodyException``|It extends ``Exception``
``TooManyArmsException``|It extends ``BodyException``
``TooManyLegsException``|It extends ``BodyException``

### The body parts

We'll play with *inheritance*, that's why you have to create several
*abstract classes*. All these *classes* are created in the ``isen.java2.interfaces.bodypart`` *package*.

Class|Description
-----------------------------------|-----------------------------------
 ``PrintableBodyPart``                 | <ul><li>This is the first *abstract class* to create.</li><li> It has to override the ``toString()`` *method*, which returns the following ``string``:  ``Part:<name of the body part>``</li><li> *Tip: in order to get the ``name`` of the ``body part``, you just have to call* ``this.getClass().getSimpleName()`` *inside your method.* </li></ul>
``Head``|A *child* of ``PrintableBodyPart``
``Trunk``|Same as ``Head``
``Limb`` |<ul><li>Second *abstract class*! You have to make it extend the ``PrintableBodyPart`` *class*.</li><li> Give it an *attribute* ``jointName`` with a getter *method* ``getJointName()``</li><li>Add a *constructor* with the ``jointName`` as a *parameter*</li><li>Implement the ``toString()`` *method* so that any child of this *class* returns its ``name`` and its ``joint``</li></ul>
``Leg``|It extends ``Limb`` with a *"knee"* joint name.
``Arm``|It extends ``Limb`` with a *"elbow"* joint name.

Try to create and print some of these *classes*.

### Species

Create an ``Enum`` *class* called ``Family`` in the ``species`` *package*. It will have the following *values*: *MAMMAL, REPTILE, BIRD, FISH, and AMPHIBIAN*.

Create an *abstract class* ``Species`` in the ``isen.java2.interfaces.species`` *package*. This *class* must have the following *abstract methods*:

Name|Parameters|Return type|Exceptions
---|---|---|---
``getFamily``||``Family``|
``isExtinct``||``Boolean``|
``addLimb``|``A Limb``||``BodyException``

You can test that it is impossible to create an *object* from the ``Species`` *class*.

### Human

Create a ``Human`` *class* in the ``isen.java2.interfaces.species.impl`` *package* that extends the ``Species`` abstract *class*. Implement the ``getFamily`` and ``isExtinct`` *methods*.

A ``human`` should have the following *attributes*:

- a ``String`` named ``name``
- a ``Leg`` named ``leftLeg``
- a ``Leg`` named ``rightLeg``
- an ``Arm`` named ``leftArm``
- an ``Arm`` named ``rightArm``
- a ``Head``
- a ``Trunk``

Create the ``toString()`` *method* thanks to your favorite *IDE* tooling. It should give information about all its *attributes*.

Create three setters for the ``name``, ``Head`` and ``Trunk`` *attributes*.

Create an ``addArm(...)`` private *method*:

- it takes one *parameter*: an ``Arm``
- it returns ``void``
- it *throws* the ``TooManyArmsException``
- if ``leftArm`` is ``null``, it sets ``leftArm`` with the *parameter*
- if ``leftArm`` is not ``null``, it sets ``rightArm`` with the *parameter*
- if ``leftArm`` and ``rightArm`` are not ``null``, it *throws* the correct `exception`

Create an ``addLeg(...)`` *private method* that is similar to ``addArm(...)`` but for legs.

Implement the ``addLimb(...)`` *method* that calls ``addArm(...)`` or ``addLeg(...)`` depending of the type of the *parameter*.

It's time to create a ``human``. Instanciate a ``Human`` *class* and fill all its *attributes*. Try to add too much ``limb`` to see what happens.

### Another animal

Create a new *class* in the ``isen.java2.interfaces.species.impl`` *package* for another ``animal`` like a ``cat`` or a ``diplodocus``. Implement all its logic. For exemple, a ``cat`` is very similar to a ``human`` but it has 4 ``legs`` and no ``arms``.

### TalkAble

Create a ``TalkAble`` *interface* in the ``species`` *package*. This *interface* must have the following *method*:

  Name|Parameters|Return type|Exceptions
  ------|------------|-------------|------------
  ``talk``|``String``

Make the ``Human`` *class* implements the ``TalkAble`` *interface*. The ``talk`` *method* should print: ``<name> says:`` followed by the *parameter*.

Create a ``Robot`` *class* that is not a ``Species`` but can ``talk``. The ``talk`` *method* should print the same thing that the one for a ``human`` but all in uppercase, with no punctuation and with space replaced with "-".

You can test your *classes* by using this little *method*:

```java
private void playStarWarsScene(TalkAble luke, TalkAble vader) {
  vader.talk("There is no escape. Don't make me destroy you. You do not yet realize your importance. You have only begun to discover your power. Join me and I will complete your training. With our combined strength, we can end this destructive conflict and bring order to the galaxy.");
  luke.talk("I'll never join you!");
  vader.talk("If you only knew the power of the dark side. Obi-Wan never told you what happened to your father.");
  luke.talk("He told me enough! It was you who killed him.");
  vader.talk("No. I am your father.");
  luke.talk("No. No. That's not true! That's impossible!");
  vader.talk("Search your feelings. You know it to be true.");
  luke.talk("No! No! No!");
  vader.talk("Luke. You can destroy the Emperor. He has foreseen this. It is your destiny. Join me, and together we can rule the galaxy as father and son. Come tith me. It is the only way.");
}
```

!!! hint
    Use `Alt`+`Shift`+`X`,`J` in Eclipse to launch your Application class

### Factories

-----

**_Factories_** are *classes* and *methods* that will encapsulate the logic to create *objects*.

Create in the ``isen.java2.interfaces.factory`` *package* an *interface* called ``SpeciesFactory`` with the following *method*:

  Name|Parameters|Return type|Exceptions
  --------------|------------|-------------|---------------
  ``createAnimal``||``Species``|``BodyException``

Create two *implementations* of the ``SpeciesFactory``, one to create ``human`` called ``HumanFactory`` and one for your other ``animal``. In the *implementation* of the ``createAnimal()`` *method*, create the ``animal`` by instantiating the correct *class* and adding all the necessary *attributes*.

Use your **_factories_** to create *instances* of your *classes*.

!!! hint
    To refactor your code in Eclipse, you can (should) use the `Alt`+`Shift`+`T` shortcut to save time.
### Singleton pattern

The **_singleton_** pattern is a design pattern that aims to limit the number of instantiation of a *class* to one *object*. In our application, only one *factory* of each kind is necessary. So, we will make them into **_singleton_**.

Here is an example of an *implementation* of the **_Singleton_** pattern for the
``HumanFactory`` *class*:

```java
public class HumanFactory implements SpeciesFactory {

  private static class HumanFactoryHolder {
    private static final HumanFactory INSTANCE = new
    HumanFactory();
  }

  public static HumanFactory getInstance() {
    return HumanFactoryHolder INSTANCE;
  }

  private HumanFactory() {
    super();
  }

  (...)

}
```

To access to the only *implementation* of our **_singleton_** *class*, the ``getInstance()`` *method* is used.

```java
HumanFactory.getInstance();
```

Try your new **_singleton_** **_factories_**.

## That's all folks!

-----

With this work, you now remember how to:

- create *packages*
- create *classes*
- create *abstract* *classes*
- play with *inheritance*
- play with *exceptions*

And we have started to use *interfaces*.
