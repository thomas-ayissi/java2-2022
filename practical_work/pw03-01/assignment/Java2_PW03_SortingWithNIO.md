# Practical Work 03-1: Sorting with the NIO API

## Intro

We will use the NIO API from JDK in order to sort files directly in your hard drive!

## Instructions

### Creation of the project

Inside your IDE, create a new Java Project.

!!! warning
    The minimum required version of Java is JDK 11

On your hard drive, create a directory like: `c:/isen/java2/nio`

You can then get the [`java2_pw03_animals.zip`](./java2_pw03_animals.zip) file by downloading it or cloning the course repository, and then unzip the ~2000 files inside the `animal` subdirectory in `c:/isen/java2/nio`

### Creation of the *packages* and application

You have to create the following *packages* in your project:

* `isen.java2.nio.app`
* `isen.java2.nio.sorter`

Create an `Application` *class* in the `app` *package* with a `main` *method*. You will use this *class* to test your work throughout the creation of your project.

### FileSorter

Create a *class* named `isen.java2.nio.sorter.FileSorter`

#### Attributes

This *class* has 4 private *attributes*:

* a `Path` named `root`
* a `Path` named `archive`
* a `Path` named `byExtension`
* a `Path` named `animals`

We will initialize them later thanks to the *constructor*.

#### Utility Methods

##### prepareDirectory

Write a public *method* `#!java prepareDirectory(...)` with the following
specification:

* it returns a Path
* it throws IOException
* it takes 2 arguments:
    * newDir, a String
    * base, a Path

Its implementation follows this rule:

> it creates a `Path` "*by resolving the newDir from the base*". If the created `Path` does not exist, the *method* creates it in the file system. Finally, the *method* returns the `Path`.

Test your *method* with the following code. After the execution, a new directory `testDirectory` should be created next to the `animal` directory you created previously. If you relaunch your application, it should do nothing on the filesystem and generate no error.

```java
FileSorter sorter = new FileSorter();
sorter.prepareDirectory("testDirectory",Paths.get("c:/isen/java2/nio"));
```

##### getExtension

Write a public *method* getExtension(...) with the following specification:

* it returns a String
* it takes 1 argument:
    * `entry`, a `Path`

Its implementation follows this rule:

> From the `entry`, it gets the filename then returns a substring which corresponds to everything after the last dot (".") of the filename.

Test your *method* with the following code. After the execution, it should have printed `png` in the console.

```java
FileSorter sorter = new FileSorter();
System.out.println(sorter.getExtension(Paths.get("c:/isen/java2/nio/test.png")));
```

##### copyFile

Write a public *method* `copyFile(...)` with the following specification:

* it returns nothing
* it throws `IOException`
* it takes 2 arguments:
    * `entry`, a `Path`
    * `directory`, a `Path`

Its implementation follows this rule:

> Inside the `directory`, it resolves the filename of the `entry` in order to build the target of the copy. Finally, it copies the `entry` to the built target.

Test your *method* with the following code. After the execution, a file `adelie-penguin.html` should be next to the `animal` directory.

```java
FileSorter sorter = new FileSorter();
sorter.copyFile(Paths.*get*("c:/isen/java2/nio/animal/adelie-penguin.html"),Paths.get("c:/isen/java2/nio"));
```

##### moveFileToArchive

Write a public *method* `moveFileToArchive(...)` with the following specification:

* it returns nothing
* it throws `IOException`
* it takes 1 arguments:
    * `entry`, a `Path`

Its implementation follows this rule:

> Inside the `archive` attribute, it resolves the filename of the `entry` in order to build the target of the copy. Finally, it moves the entry to the target.

To test this *method*, it is necessary to have a *setter* for the `archive` *attribute* and to create the directory `archive` next to the `animal` directory. Create the *setter* and test your  *method* with the following code. After the execution, the file `adelie-penguin.html` created by the previous *method* should have been moved to the `archive` directory you just created.

```java
FileSorter sorter = new FileSorter();
sorter.setArchive(Paths.get("c:/isen/java2/nio/archive"));
sorter.moveFileToArchive(Paths.get("c:/isen/java2/nio/adelie-penguin.html"));
```

#### Constructor

The `FileSorter` *class* has one *constructor* which takes a `String` (the root directory) as *parameter* and throws `IOException`.

* It initializes the `root` *attribute* with a `Path` pointing to the root directory
* It initializes the `animals` *attribute* with a `Path` pointing to the `animal` directory inside the root directory
* It initializes the `archive` *attribute* with a `Path` pointing to the `archive` directory inside the root directory, and creates it if necessary (remember `prepareDirectory`!)
* It initializes the `byExtension` *attribute* with a `Path` pointing to the `byext` directory inside the root directory, and creates it if necessary

#### Final Method: sortFiles

The `sortFiles` *method* is public, throws `IOException` and returns an `integer`.

It reads the content of the `animals` `Path`. For each `entry` of this directory:

* it gets the filename of the entry then deduce the extension;
* it creates if necessary the folder with the extension name in the `Path` `byExtension` (remember `prepareDirectory`!);
* it copies the entry inside the created folder;
* it moves the entry to the archive;
* it returns the number of entry sorted.

A the end of the process, all the css files are in `c:/isen/java2/nio/byextension/css/`, the png ones in `c:/isen/java2/nio/byextension/png/`, etc... All the files are moved from
`c:/isen/java2/nio/animals` to `c:/isen/java2/nio/archive`

Do not hesitate to create this *method* in several steps. By example, you could first only print in the console the files in the animal directory. Then you could just create the extension directories. Etc.

## Bonus

* Stage 1 : If you have enough motivation, you can change your program to make it read a property file in order to retrieve the root directory path.
* Stage 2 : Is your code documented ? is it null-safe ?
