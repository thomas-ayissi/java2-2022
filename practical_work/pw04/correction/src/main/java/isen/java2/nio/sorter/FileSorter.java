package isen.java2.nio.sorter;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Philippe Duval
 *
 *         This is a class we already know, except that it contains errors.
 *         Notice that the methods not dealing with actual use, i.e the methods
 *         that are internal implementation of the class, are now protected, for
 *         the TestCase to access them, while the rest of the world can't.
 */
public class FileSorter {

	private static final String ARCHIVE_DIR = "archive";
	private static final String BY_EXT_DIR = "byext";
	private static final String ANIMALS_DIR = "animals";

	private Path root;
	private Path archive;
	private Path byExtension;
	private Path animals;

	public FileSorter(String rootDir) throws FileSortException {
		try {
			root = Paths.get(rootDir);
			// Obviously there was something missing there. Did your tests find
			// it ?
			animals = root.resolve(ANIMALS_DIR);
			archive = prepareDirectory(ARCHIVE_DIR, root);
			byExtension = prepareDirectory(BY_EXT_DIR, root);
		} catch (IOException e) {
			throw new FileSortException("Directories can't be created.", e);
		}
	}

	protected Path prepareDirectory(String newDir, Path base) throws IOException {
		Path path = base.resolve(newDir);
		// Oops, (intentional) typo. Normally the compiler tells you that there
		// is something wrong...
		System.out.println("Creating " + newDir + "...");
		// We should verify that the path does not already exist, or else some
		// tests will fail
		if (Files.notExists(path)) {
			Files.createDirectory(path);
		}
		return path;
	}

	public int sortFiles() throws FileSortException {
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(animals)) {
			int count = 0;
			for (Path entry : stream) {
				String extension = getExtension(entry);
				Path directory = prepareDirectory(extension, byExtension);
				copyFile(entry, directory);
				moveFileToArchive(entry);
			}
			return count;
		} catch (IOException e) {
			throw new FileSortException("Some error has appeared during the sorting.", e);
		}
	}

	protected void moveFileToArchive(Path entry) throws IOException {
		String filename = entry.getFileName().toString();
		Files.move(entry, archive.resolve(filename));
	}

	protected void copyFile(Path entry, Path directory) throws IOException {
		String filename = entry.getFileName().toString();
		Path copy = directory.resolve(filename);
		// Naturally, the code below needed some work before compiling.
		Files.copy(entry, copy);
	}

	protected String getExtension(Path entry) {
		String fileName = entry.getFileName().toString();
		// This had to be corrected for the tests to pass. Did your tests find
		// it ?
		String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
		return extension;
	}

}
