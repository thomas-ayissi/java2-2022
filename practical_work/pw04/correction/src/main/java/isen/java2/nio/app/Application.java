package isen.java2.nio.app;

import java.io.IOException;
import java.util.Properties;

import isen.java2.nio.sorter.FileSortException;
import isen.java2.nio.sorter.FileSorter;

/**
 * @author Philippe Duval
 *
 *	This class becomes totally useless, as we will do all our test from the TestCases
 */
public class Application {

	public static void main(String[] args) throws FileSortException {
		try {
			FileSorter fileSorter = new FileSorter(getRootDir());
			int count = fileSorter.sortFiles();
			System.err.println("Played with " + count + " files !");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static String getRootDir() throws IOException {
		Properties properties = new Properties();
		properties.load(Application.class.getClassLoader().getResourceAsStream("nio.properties"));
		return properties.getProperty("rootDir");
	}

}
