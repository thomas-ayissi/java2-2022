package isen.java2.pagebuilder.builder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class PageBuilder {

	private Path root;

	private Path outputFile;

	private Path startFile;

	public PageBuilder(Path startFile, Path outputFile) {
		this.startFile = startFile;
		root = startFile.getParent();
		this.outputFile = outputFile;
	}

	public void build() throws IOException {
		try (BufferedWriter writer = Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8)) {
			writeFileContent(startFile.getFileName().toString(), writer);
			writer.flush();
		}

	}

	public void writeFileContent(String filename, BufferedWriter writer) throws IOException {
		System.err.println("reading " + filename + "...");
		try (BufferedReader fileReader = Files.newBufferedReader(root.resolve(filename))) {
			String line;
			while ((line = fileReader.readLine()) != null) {
				processLine(writer, line);
			}
		}
	}

	protected void processLine(BufferedWriter writer, String line) throws IOException {
		String inclusion = getFileToInclude(line);
		if (inclusion != null) {
			writeFileContent(inclusion, writer);
		} else {
			writer.write(line);
			// this will work better if we insert new lines ! For this I modified the signature of my method.
			// As I have tests, I can rely on them to be sure that I don't break everything !
			writer.newLine();
		}
	}

	protected String getFileToInclude(String line) {
		line = line.trim();
		// Oops, there was a (intentional) typo. Did your tests find it ?
		if (line.startsWith("[[") && line.endsWith("]]")) {
			return line.substring(2, line.length() - 2);
		} else {
			return null;
		}
	}

}
