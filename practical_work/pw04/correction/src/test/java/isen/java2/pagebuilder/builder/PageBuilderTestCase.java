package isen.java2.pagebuilder.builder;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Philippe Duval
 * 
 *         Test class used to test the behavior of the PageBuilder Class. This
 *         class replaces the application class we used before, and becomes our
 *         entrypoint into the program
 */
public class PageBuilderTestCase {

	private PageBuilder pageBuilder;
	// You can adapt the path on your filesystem here
	private String rootDir = "c:/tmp/java2/buildertest/";

	/**
	 * @throws Exception
	 *             Method used to create our controlled environment, called
	 *             every time a test runs
	 */
	@Before
	public void initTests() throws Exception {
		// Creating the test directory
		Path rootDirPath = Paths.get(rootDir);
		Files.createDirectories(rootDirPath);
		// Adding test files
		// Here we copy the testfiles that are provided in the project's
		// test/resources directory to the test rootDir specified above. This
		// differs from the other test, as here we need to setup what's inside
		// the files
		InputStream testFileInputStream = this.getClass().getClassLoader().getResourceAsStream("file.txt");
		Files.copy(testFileInputStream, rootDirPath.resolve("file.txt"));
		InputStream inclusionFileInputStream = this.getClass().getClassLoader().getResourceAsStream("inclusion.txt");
		Files.copy(inclusionFileInputStream, rootDirPath.resolve("inclusion.txt"));

		pageBuilder = new PageBuilder(rootDirPath.resolve("file.txt"), rootDirPath.resolve("output.txt"));
	}

	/**
	 * @throws Exception
	 *             Method used to cleanup our environment, i.e. delete the
	 *             copied files, called everytime a test ends
	 */
	@After
	public void deleteTestFiles() throws Exception {
		Files.delete(Paths.get(rootDir, "file.txt"));
		Files.delete(Paths.get(rootDir, "inclusion.txt"));
		if (Files.exists(Paths.get(rootDir, "output.txt"))) {
			Files.delete(Paths.get(rootDir, "output.txt"));
		}
	}

	@Test
	public void shouldGetNull() {
		// GIVEN
		String line = "Test line";
		// WHEN
		String fileToInclude = pageBuilder.getFileToInclude(line);
		// THEN
		assertThat(fileToInclude).isNull();
	}

	@Test
	public void shouldGetFileToInclude() {
		// GIVEN
		String line = "[[filetoinclude.txt]]";
		// WHEN
		String fileToInclude = pageBuilder.getFileToInclude(line);
		// THEN
		assertThat(fileToInclude).isNotNull();
		assertThat(fileToInclude).isEqualTo("filetoinclude.txt");
	}

	@Test
	public void shouldWriteContentInWriter() throws Exception {
		// GIVEN
		String fileName = "inclusion.txt";
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(rootDir, "output.txt"));
		// WHEN
		pageBuilder.writeFileContent(fileName, writer);
		writer.close();
		// THEN
		List<String> lines = Files.readAllLines(Paths.get(rootDir, "output.txt"));
		assertThat(lines).hasSize(1);
		assertThat(lines).containsOnly("Inclusion Line");
	}

	@Test
	public void shouldWriteLineInWriter() throws Exception {
		// GIVEN
		String line = "my test line";
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(rootDir, "output.txt"));
		// WHEN
		pageBuilder.processLine(writer, line);
		writer.close();
		// THEN
		List<String> lines = Files.readAllLines(Paths.get(rootDir, "output.txt"));
		assertThat(lines).hasSize(1);
		assertThat(lines).containsOnly("my test line");
	}

	@Test
	public void shouldWriteFileContentInWriter() throws Exception {
		// GIVEN
		String line = "[[inclusion.txt]]";
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(rootDir, "output.txt"));
		// WHEN
		pageBuilder.processLine(writer, line);
		writer.close();
		// THEN
		List<String> lines = Files.readAllLines(Paths.get(rootDir, "output.txt"));
		assertThat(lines).hasSize(1);
		assertThat(lines).containsOnly("Inclusion Line");
	}

	@Test
	public void shouldBuildFinalFile() throws Exception {
		// GIVEN all we have already in the @Before method.
		// Again, this is our "main" method in the class, so we should not have
		// to setup details other than ensuring that the files are at the right
		// place
		
		// WHEN we build the page (here we do only ONE thing)
		pageBuilder.build();
		
		// THEN we should have 4 lines, containing the included files
		List<String> lines = Files.readAllLines(Paths.get(rootDir, "output.txt"));
		assertThat(lines).hasSize(4);
		assertThat(lines).contains("First Line");
		assertThat(lines).contains("Inclusion Line");
		// we could add a test checking that we don't have files to include
		// anymore in the base template, for example. feel free to make any
		// assert that you think is useful to describe the expected result

	}
}
