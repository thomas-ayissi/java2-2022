package isen.java2.nio.sorter;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Philippe Duval
 * 
 *         Test class used to test the behavior of the FileSorter Class. This
 *         class replaces the application class we used before, and becomes our
 *         entrypoint into the program
 */
public class FileSorterTestCase {

	private FileSorter fileSorter;
	// You can adapt the path on your filesystem here
	private String rootDir = "c:/tmp/java2/niotest/";

	/**
	 * @throws Exception
	 *             Method used to create our controlled environment, called
	 *             every time a test runs
	 */
	@Before
	public void initTests() throws Exception {
		// Creating the test directory
		Path rootDirPath = Paths.get(rootDir);
		Files.createDirectories(rootDirPath);
		this.initAnimalsDirectory(rootDirPath);

		fileSorter = new FileSorter(rootDir);
	}

	/**
	 * @throws Exception
	 *             Method used to cleanup our environment, i.e. delete the
	 *             copied files, called everytime a test ends
	 */
	@After
	public void purgeTestDirectory() throws Exception {
		this.purgeDirectory(Paths.get(rootDir));
	}

	@Test
	public void shouldGetExtension() {
		// GIVEN
		Path fileName = Paths.get("test.png");
		// WHEN
		String extension = fileSorter.getExtension(fileName);
		// THEN
		assertThat(extension).isEqualTo("png");
	}

	@Test
	public void shouldPrepareDirectoryAndReturnExistingPath() throws Exception {
		// GIVEN
		Path mainDir = Paths.get(rootDir);
		String newDirName = "animals";
		// WHEN
		Path newDirPath = fileSorter.prepareDirectory(newDirName, mainDir);
		// THEN
		assertThat(Files.exists(newDirPath)).isTrue();
		assertThat(Files.isDirectory(newDirPath)).isTrue();
		assertThat(newDirPath.getFileName().toString()).isEqualTo("animals");
	}

	@Test
	public void shouldPrepareDirectoryAndReturnNewPath() throws Exception {
		// GIVEN
		Path mainDir = Paths.get(rootDir);
		String newDirName = "testDir";
		// WHEN
		Path newDirPath = fileSorter.prepareDirectory(newDirName, mainDir);
		// THEN
		assertThat(Files.exists(newDirPath)).isTrue();
		assertThat(Files.isDirectory(newDirPath)).isTrue();
		assertThat(newDirPath.getFileName().toString()).isEqualTo("testDir");
	}

	@Test
	public void shouldCopyFile() throws Exception {
		// GIVEN
		Path targetDir = Paths.get(rootDir);
		Path entry = Paths.get(rootDir, "animals/test.png");
		// WHEN
		fileSorter.copyFile(entry, targetDir);
		// THEN
		assertThat(Files.exists(Paths.get(rootDir, "test.png"))).isTrue();
		assertThat(Files.exists(entry)).isTrue();
	}

	@Test
	public void shouldMoveFileToArchive() throws Exception {
		// GIVEN
		Path entry = Paths.get(rootDir, "animals/test.png");
		// WHEN
		fileSorter.moveFileToArchive(entry);
		// THEN
		assertThat(Files.exists(Paths.get(rootDir, "archive/test.png"))).isTrue();
		assertThat(Files.notExists(entry)).isTrue();
	}

	@Test
	public void shouldSortFiles() throws Exception {
		// GIVEN all we already have in the initAnimalDirectory method
		// Notice that here we don't have to setup much boilerplate code, as we
		// call the "global" working method of our class under test.

		// WHEN we sort the files
		fileSorter.sortFiles();

		// THEN the files should be sorted in the right directories, archived,
		// and no more in the initial directory
		assertThat(Files.exists(Paths.get(rootDir, "byext/png/test.png"))).isTrue();
		assertThat(Files.exists(Paths.get(rootDir, "byext/gif/test.gif"))).isTrue();
		assertThat(Files.exists(Paths.get(rootDir, "archive/test.gif"))).isTrue();
		assertThat(Files.exists(Paths.get(rootDir, "archive/test.gif"))).isTrue();
		assertThat(Files.exists(Paths.get(rootDir, "animals/test.gif"))).isFalse();
		assertThat(Files.exists(Paths.get(rootDir, "animals/test.gif"))).isFalse();
	}

	/**
	 * @param rootDir
	 * @throws Exception
	 *             This method is a helper method used to initialize our test
	 *             environment. You don't have to have files on your disk prior
	 *             to executing the test, this method takes care of the job for
	 *             you
	 */
	private void initAnimalsDirectory(Path rootDir) throws Exception {
		// Create animals directory if does not exist
		Path animalsPath = rootDir.resolve("animals");
		Files.createDirectory(animalsPath);

		// Create some files in animals directory
		Files.createFile(animalsPath.resolve("test.png"));
		Files.createFile(animalsPath.resolve("test2.png"));
		Files.createFile(animalsPath.resolve("test.gif"));
		Files.createFile(animalsPath.resolve("test2.gif"));
		Files.createFile(animalsPath.resolve("test.html"));
	}

	/**
	 * @param dir
	 * @throws Exception
	 *             This method is a helper used to cleanup our test environment
	 * 
	 */
	private void purgeDirectory(Path dir) throws Exception {
		Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}

		});
	}
}
