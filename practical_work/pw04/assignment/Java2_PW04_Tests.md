# Practical Work 04: soooo many errors!

## Intro

-----

We will take an existing project and correct all the errors in it. For that purpose, we will write and execute some unit tests and use our IDE to help us.

## Installation

-----

The first step is to download the starting project [``java2_pw04_error_solving.zip``](./java2_pw04_error_solving.zip).

Extract the downloaded file on your disk and import the project inside your *IDE*, as a *maven* project.

!!! hint
    In Eclipse, use the `File -> Import` wizard, and choose `existing maven project` as type.

!!! warning
    If you don't import the project as a **maven** project, don't expect it to work, as nothing will download the dependencies magically !

## Instructions

-----

### A project full of problems

The project looks like what we wrote last week. But it has been completely filled it with mistakes. Your goal is to *correct all these mistakes*.

#### Compilation Errors

The first errors to try to solve when you work on a Java program are the `compilation errors`.

!!! hint
    In Eclipse, the compilation errors can be detected thanks to the red markers on the files, or in the *Problems View* that can be summoned using ++alt+shift+'q'++,++'k'++.

Open the `FileSorter` Java class. In the Java editor, each compilation error is underlined in red and a red cross is present at the start of the line with an error. By hovering the mouse cursor on an error, a tip is shown. Try to fix all the errors in the *class* and also in the ``FileSorterTestCase`` *class* situated in the `src/test/Java` source directory.

!!! hint
    In Eclipse, you can use ++ctrl+shift+'1'++ to apply quick fixes

#### Import errors

Import errors are a particular kind of compilation error. By default, a Java *class* can use *classes* that are in its package. To use other classes, it is necessary to make an import.

An import line is written at the top of the *class* file before the *class* declaration. These import lines are **never** written by the developer! The *IDE* can write it itself.

!!! hint
    To do that in Eclipse, click at the end of the *class* you want to import in your Java editor and press ++ctrl+shift+'1'++ to open the quick fix tooltip and select ``import``.

    Another easier way to Another easier way to do it is the *Add import* shortcut, summoned by ++ctrl+shift+'m'++.

    But the killer secret combo si ++ctrl+shift+'o'++ for *Organize imports*. It keeps everything tidy once correctly imported

If nothing is suggested by the *IDE*, it means that the Java *class* that you want to use is not in the Java *classes* that your project can use. A Java project has access:

- To its own *classes* (``FileSorter`` for instance);
- To the JRE *classes* (``String`` or ``IOException`` for instance);
- To the *classes* from the *libraries* used by the project (``@Before`` for
    instance).

If you expect a specific *class* and the *IDE* does not find it, *it may be because you are missing the corresponding library...*

#### Adding a library to a maven project

To add a library to our project, open the `pom.xml` file. In this file we will add *dependencies* like explained in the lesson. Our project needs 2 *libraries*:

_|JUnit|AssertJ
:---:|:---:|:---:
groupId|junit|org.assertj
artifactId|junit|assertj-core
version|4.12|3.2.0

#### Runtime errors

Once there are no longer any compilation errors in our Java project, it does not mean that there is no compilation error left.

Runtime errors are found when running our project. Try to run the ``Application`` *class* in the ``isen.java2.nio.app`` *package* and the ``Builder``
class in the ``isen.java2.pagebuilder.app`` *package*. Errors are printed in the console.

!!! hint
    To run a class in Eclipse, select the class in the outline or the package explorer and use ++alt+shift+'X'++,++'J'++

Run the 2 test *classes*. Errors are also printed for some tests.

!!! hint
    To run a test case in Eclipse, select the test class in the outline or the package explorer and use ++alt+shift+'X'++, ++'T'++

#### Stack traces

!!! info
    this is a reminder, you can jump this section if you have just followed the course

In Java, runtime errors are represented as a *stack trace*, e.g.:

```java hl_lines="1 5"
isen.java2.library.exceptions.ItemAlreadyBorrowedException: Item already borrowed !
    at isen.java2.library.Library.borrowItem(Library.java:121)
    at isen.java2.library.Library.borrowBook(Library.java:100)
    at isen.java2.library.Application.main(Application.java:91)
Caused by: java.lang.NullPointerException
    at isen.java2.library.Library.borrowItem(Library.java:120)
    ... 2 more
```

It is, as the name implies, the list of all the methods involved in the current stack call.

Reading a stack trace is an important skill for a Java developer. A stack trace list the `exception` that has been raised in your code. It tells what `exception` (``isen.java2.nio.sorter.ItemAlreadyBorrowedException``) with its message (*Item already borrowed !*) and a list of the *methods* the exception have been thrown through. For each method, it is possible to see at which line in the code it has been thrown (``Library.java:120``). Depending on your *IDE*, these line numbers can be clickable so that you can get to the buggy code directly.

The ``Caused by`` line explains that the ``ItemAlreadyBorrowedException`` has been thrown in a catch block that caught a ``NullPointerException``. A stack trace can have multiple ``Caused by`` lines. The lowest of these lines is the *Root* ``exception``, which is **the one you should be solving**.

When reading a stack trace, the following process can be done:

- If there are more than one stack trace, look at the first one;
- Find the root cause of the stack trace;
- Read its message and try to understand it;
- Find in the first *method* listed below the root cause that is in a *class* that you wrote;
- Go to the line in your code to fix the problem!

#### Fixing and writing tests

Try to understand why some tests do not work and fix the problems.

Two tests (``shouldBuildFinalFile()`` and ``shouldSortFiles()``) are not implemented so you have to write them.

## Bonus stage

-----

Now that you are confident in using test cases to dig into Java problems and that you know how to read a stack trace, you are up to the challenge of completing the *Gilded Rose* kata.

*What is a kata ?*, you may ask ! This is a concept inherited from the martial arts : an exercise that you can use to practice your skills. It is intended to be done again and again, following your progress. You will not succeed at first try, and you will find new solutions every time you go through the exercise. More on kata and the concept of coding dojo can be found [here](https://codingdojo.org/).

Some basic rules to execute a kata :

- there is no unique solution
- the process is more important than the result
- trash your code once you are done
- you can choose a constraint (time, no mouse...), and test it on a known kata
- you can choose an new thing to learn, and test it on a known kata

To begin the kata, just clone the repository from the course, head to the `practical_work/pw4/bonus` directory, and read the instructions in the `GildedRoseRequirements.txt` file.

You can find the original source code, in other languages, and a lot of other kata on the Github of [Emily Bache](https://github.com/emilybache).
