# Practical Work 03-2: Building a web page

## Intro

Let's play with IOStreams in order to read and write files. At the end of this practical, a web page will be automatically built thanks to your work!

You will read one file, line by line. For each analysed line, you will have to detect if you have to include the content of another file, recursively. Everything you read must be written in an output file.

```mermaid
flowchart LR
f1[file to include]
f2[file to include]
f3[file to include]
f4[file to include]
f5[file to include]
if[input file]
prog(Your Program)
of[output file]
  f3-.->f2
  f4-.->f2
  f5-.->f2
  f2-.->if
  f1-.->if
  if-->prog
  prog-->of
  style f1 fill:#fff
  style f2 fill:#fff
  style f3 fill:#fff
  style f4 fill:#fff
  style f5 fill:#fff
  style if fill:#fff
  style of fill:#ccc
```

When a line begins with `[[` and ends with `]]` (ex : `[[path/to/another/file/to/include]]`), it means that you have to replace it by the content of the file to include.

## Instructions

### Let's create a new project

As usual, create a new project in your editor.

On your hard drive, create a directory like: `c:/isen/java2/nio/pagebuilder`

You can then get the [`java2_pw03_pagebuilder.zip`](./java2_pw03_pagebuilder.zip) file by downloading it or cloning the course repository, and then unzip the files inside the `c:/isen/java2/nio/pagebuilder` directory

### Creation of the *packages* and application

You have to create the following *packages* in your project:

* `isen.java2.pagebuilder.app`

* `isen.java2.pagebuilder.builder`

Create an `Application` *class* in the app *package* with a `main` *method*. You will use this *class* to test your work throughout the creation of your project.

### PageBuilder

Create a *class* named `isen.java2.pagebuilder.builder.PageBuilder`

#### Attributes

This *class* has 3 private attributes:

* a `Path` named `root`
* a `Path` named `startFile`
* a `Path` named `outputFile`

#### Constructor

The `PageBuilder` *class* has one *constructor* which takes 2 `Path` as parameter, the `startFile` and the `outputFile`.

* It initializes the `root` *attribute* by getting the *parent* of the `startFile`
* It fills the `startFile` *attribute* with the `startFile` *parameter*
* It fills the `outputFile` *attribute* with the `outputFile` *parameter*.

#### Methods

##### build

Write a public *method* `build()` with the following specification:

* it returns nothing
* it throws `IOException`
* it takes no argument

The `build()` method follows this specification:

> 1. It instantiates a `BufferedWriter` from the `outputFile` *attribute*. The charset we will use is UTF-8.
> 2. It writes "I'm the first line written in this file" in the `outputFile`.
> 3. It flushes the `Writer`

This *method* will be our entry point for the building of our HTML page.

### Application

In the main *method* of the `Application` *class*, instantiates the `PageBuilder` with

* the startFile equal to c:/isen/java2/nio/pagebuilder/index.html
* the outputFile equal to c:/isen/java2/nio/pagebuilder/output.html

It finally calls the `build()` *method* of the builder (You'll have to handle the `exception` properly). Launch your program and look at the `output.html` file that you just created. It should have our line written in it!

### Back to our builder

##### writeFileContent

Write a public *method* `writeFileContent(...)` with the following specification:

* it returns nothing
* it throws `IOException`
* it takes 2 arguments:
    * `filename`, a `String`
    * `writer`, a `Writer`

The `writeFileContent(...)` method follows this specification:

> 1. It prints into the Java Console the filename being written.
> 2. It creates a `Path` by resolving the filename from the root.
> 3. It creates a `BufferedReader` with this `Path`
> 4. While the `BufferedReader` can read lines, it writes the line in the `writer`.

Modify your `build()` *method* and replace the line that writes `"I'm the first line written in this file"`. You must now call the `writeFileContent(...)` *method* with the   as the `fileName` *parameter* and the `writer`.

Relaunch your program. Now the `output.html` file should have the same content than the start file.

##### getFileToInclude

Write a public *method* `getFileToInclude(...)` with the following specification:

* it returns a `String`
* it takes 1 argument:
    * `line`, a `String`

It cleans the `String` parameter, by deleting the spaces at the beginning and at the end (take a look at `.strip()` of the `String` class)

If the `String` *parameter* starts with `[[` and ends with `]]`, it returns the content between these two special brackets, otherwise it returns `null`.

If you want to test, you can proceed like this:

```java
PageBuilder builder = new PageBuilder();

System.out.println(builder.getFileToInclude("test line")); // prints null
System.out.println(builder.getFileToInclude("[[path/to/file]]")); // prints path/to/file
```

Now we can detect if a line in the start file is to be included directly or not!

##### processLine

Write a public *method* `processLine(...)` with the following specification:

* it returns nothing
* it throws `IOException`
* it takes 2 arguments:
    * `writer`, a `Writer`
    * `line`, a `String`

It calls `getFileToInclude(...)` with the `line` *parameter*. If the returned value is `null`, it writes the line with the `writer`, otherwise it opens a new `Reader` then writes its content line by line with the `writer`.

Modify the `writeFileContent(...)` *method* and modify the line when you write content in the `writer`. Your *method* should now call the `processLine(...)` *method*.

##### Recursion

The current implementation of `processLine(...)` works pretty well but imagine if we need several degrees of inclusion? A file included in a file, included in a file, included in a file, etc... This implementation can't handle such a pattern!

That's why we need to add recursion in our code structure. What if `processLine(...)` calls `writeFileContent(...)` instead of creating a new `Reader`? Modify your code and add recursion.

At this stage, your program is able to build a full HTML webpage, open `output.html` with your favorite browser and enjoy the result, congratulations!

## Bonus

* Stage 1 : If you have enough time and motivation, you can change your program to make it read a property file in order to retrieve the `startFile` and the `outputFile`.
* Stage 2 : Is your code documented ? is it null-safe ? are all the visibilities correct, i.e. is your contract meaningful ?
