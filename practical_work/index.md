# Welcome to the JAVA 2 course

This repository contains the documents that will be used during the course.

After each session, you will also be able to find a possible solution for the practical exercises.

## Prerequisites

To follow this course, you will have to use some tools, like the *Java framework* and an *IDE*. You will find here up-to-date information about which tools are required, and which versions you should use.

!!! info
    I will provide support for these tools, in these particular versions.

!!! danger "Mind your IDE"
    I **will not** provide support for other tools that you may use, especially *IDEs*, as they can differ quite a lot between each other.

!!! danger "Use the right Java version"
    I *will also not* provide support if you don't respect the version. Java, as any other language, has version numbers for a reason. Some functions are not available on older version of Java, some functions are deprecated on newer version. The same goes for libraries and tools. **Use the right version**

## Java framework

!!! info
    *TL;DR:* You will need *at least* **Java 17 SDK**. If you install the latest version of Eclipse, Java 17 JRE can be selected during the installation process. IntelliJ IDEA also provides its own included Java Runtime, just be sure to use Java 17. Otherwise, grab the [latest JDK17 version](https://download.java.net/java/GA/jdk17.0.1/2a2082e5a09d4267845be086888add4f/12/GPL/openjdk-17.0.1_windows-x64_bin.zip) on [OpenJDK](https://jdk.java.net/17/).

Java follows a strange versionning path:

- Java has regular versions, and *LTS* (long term support) versions.
- Every Java version introduces *GA (Globally available)* changes, that are ready for production, and *Preview* changes, that are, well, for preview...
- for years, versions have come at a rather slow pace, up until Java 8. These versions were introducing a lot of new features. Preview in `1.X` meant *GA* in ``1.X+1``
- from Java 9 and upwards, the rhythm as accelerated quite a lot, and we are already at Java 17 in less than four years. These version do not introduce as many features as the former ones. Preview in ``1.X`` tends to mean GA in next *LTS*.
- Java *LTS* versions are Java 8, Java 11, and Java 17.
- Java is leaded by Oracle, but is a OpenSource product. several Companies offer support on the opensource distribution, providing their own packaging. we can mention Oracle of course, but also RedHat and AWS (with Correto). All these distributions are commercially available (meaning not free !). Since Java 17, Oracle rollbacked it's position, and provides again a free implementation.  There is also a freely available opensource distribution called *OpenJDK*, which is the Java community-based build, and the one you should choose if you are not looking for commercial support.

!!! danger "Do not use Oracle"
    **Do not use** the Oracle JDK, even for private use ! Oracle has a trolling habit of asking for licence fees at random points in time...

For this course, we will focus on three things:

- *Java LTS version*, because that is the one you should use in the business context
- *GA functionalities*, because these are the one that are supported by the LTS support contract, and consequently the ones you will use in a *serious* production environment.
- *HotSpot* Virtual MAchine, as it is the closer to what you will find in real production environments

As we are not looking for commercial support in the context of this course, we will use the *OpenJDK* packaging

!!! info
    If you have understood all the above, you know that we will use Java 17, and the HotSpot VM, using the OpenJDK distribution. [Download it there](https://jdk.java.net/17/) for your favorite OS

## Integrated Development Environment

!!! info
    *TL;DR:* You will also need a Java IDE. Support for Eclipse will be provided during the course, prefer this version: [Eclipse 2021-12](https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2021-12/R/eclipse-inst-jre-win64.exe&r=1) for Win64 platforms. Other platforms builds can be found [here](https://www.eclipse.org/downloads/packages/installer).

As every other language, Java is best edited uding a suitable *IDE*. There are several IDE that you can find:

- *Eclipse*, OpenSource, maintained by the Eclipse Foundation, is both free and powerful. **This is the one I will support in the course.**
- *IntelliJ Idea*, commercial, maintained by JetBrains, is powerful and appealing, free for basic use, but advanced features require a licence. It is on some points more powerful than Eclipse, but at a price.

Next to these two IDE, you can also find :

- *Visual Studio Code*, Opensource, maintained by Microsoft. It is a swiss army knife of code edition, can do Java through the use of the RedHat Java Plugin, and can be tailored to your needs. It is far less integrated than Eclipse or Idea, and far more difficult to configure. *I will not provide support to this one, as every plugin that you use can break the whole thing...*
- *Netbeans*, OpenSource, Maintained by Apache Software Foundation, is yet another editor, less complete feature wise than the others, it  requires also less resources to run. It provides great support for Swing application development, but lacks many modern features integrated in Eclipse & Idea. *I will not provide support on this one, as it is too limited to handle maven projects correctly*

There are other commercial IDE that you can encounter in your Java learning path, just keep in mind that whatever the IDE you choose, you shoul learn how to use it extensively to be as efficient as possible.

## Some Eclipse Shortcuts that will be handy

### Navigation

- `Ctrl`+`O` *outline* to navigate through your source
- `Alt`+&#8594; or &#10141; to navigate between edition markers
- `Ctrl`+`Shift`+&#8593; or &#8595; to navigate between members (methods and attributes)
- `F12` to focus on the editor
- `Ctrl`+`PgUp` or `PgDown` to cycle between open editors
- `Ctrl`+`E` to bring up the menu of open editors (`Ctrl`+`F6` does a similar job)

### Menus

- `Alt`+`Shift`+`N` *create* menu
- `Alt`+`Shift`+`S` *source* menu
- `Alt`+`Shift`+`T` *refactor* menu
- `Alt`+`Shift`+`X` *execute* menu

### Source manipulation

- `Alt`+&#8593; or &#8595; to move blocks of text around
- `Ctrl`+`D` *delete* line
- `Ctrl`+`Shift`+`F` *format* source file
- `Ctrl`+`Shift`+`C` *comment* selection
- `Ctrl`+`Shift`+`O` *organize* imports
- `Alt`+`Shift`+`J` *generate Javadoc*

- `Ctrl`+`1` (Ctrl+Shift+& for Azerty keyboards) *quick fix* menu

... and the one and only...

- `Ctrl`+`Space` *autocomplete*
