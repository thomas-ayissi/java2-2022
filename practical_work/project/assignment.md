# Project: Contact App

## Intro

-----

You will write a *Maven* project that will connect to a *SQLite* database to read and update the table `person`.

A `JavaFX` GUI allows the user to do the following actions:

* List all persons in the database;
* Add a new person with a form;
* Update the data of existing persons;
* Delete a person.

## Minimal requirements

-----

### Functional requirements

* [x] A `JavaFX` GUI allows the user to do the following actions:
    * [x] List all persons in the database;
    * [x] Add a new person with a form;
    * [x] Update the data of existing persons;
    * [x] Delete a person.

* [x] Your application *must* provide a “backup” or “export” functionality that will take all the data in your database and store it in some files.
* [x] The [vCard](https://en.wikipedia.org/wiki/VCard) format *must* be used to store the data of a person.

### Non functional requirements

* [x] You *must* write a `Maven` project that will connect to a database to read and update the table `person`.
* [x] The database used *must* be SQLite
* [x] You *must* write tests, at least for the database access methods.

All your data will be stored in a `SQLite` database. The starting point is this table:

```sql
CREATE TABLE IF NOT EXISTS person (
    idperson INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
    lastname VARCHAR(45) NOT NULL,  
    firstname VARCHAR(45) NOT NULL,
    nickname VARCHAR(45) NOT NULL,
    phone_number VARCHAR(15) NULL,
    address VARCHAR(200) NULL,
    email_address VARCHAR(150) NULL,
    birth_date DATE NULL);
```

!!!hint
    Remember the init() method of the main `Application` in JavaFX ? It may be handy at this point...

## Going further

-----

Feel free to go beyond the minimal requirements:

* [ ] Maybe, you could add a photo for each person. Or you can search for a specific person in your interface.
* [ ] Or maybe each person can be associated with a category: is it a friend, a family member or a work acquaintance?
* [ ] And you can also filter your list and/or export by category.

!!! warning
    Don’t overdo it! 2 completed functionalities are better than 5 that are half done.

## Analysis of your work

-----

You understood that this project wraps everything you learned during the Java2 module! The quality of your code, the clarity of your implementation will be taken into account for your final grade.

* The code should be modularized in several classes.
* Every opened resource has to be closed.
* The GUI has to be clear and beautiful (or at least not ugly :fontawesome-regular-smile: ).
* Even if the whole project does not work, an analysis will be made on your whole project, your maven configuration and on the ability to import your project inside any IDE.

!!! hint
    Don’t be afraid to ask your teacher if you are stuck in your implementation.

Be smart, never forget that you do not code for you, but rather for a guy who will have to maintain your code. One smart developer once said:
> “Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live.”

In the end… If it is easy to read, it is easy to evaluate :fontawesome-regular-smile-wink:

## Deadline

-----

!!! danger "Reminder"
    You have until **March, the 13th at 23:59:59.999** to send me your work.

You have two ways to send your work:

* Send me the link to a GitHub or Gitlab project (last commit **before** the deadline)
* Send me a zip file (max 10 mb) by email ;

Your code / email have to clearly state who the persons in your work group are.

It is your responsibility to ensure that I received the file. If I received your code I will get back to you individually to confirm that I received your work no later than March, the 15th at 23:59.

Provide all the information you think is necessary to understand and rate your project. If you change the database table, don’t forget to change the `SQL` script in your project. It should be self sufficient, I won't figure out the database script from your code !

!!! info
    Whatever format you choose, remember to include the necessary files, but no more !

    Your project should at least contain:

    * [x] a `pom.xml` file
    * [x] your java classes
    * [x] your resource files (.fxml and so on)

    Your project may also contain:

    * [ ] a README.md file stating what you have done in the project
