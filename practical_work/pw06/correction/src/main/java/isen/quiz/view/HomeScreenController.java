/**
 * 
 */
package isen.quiz.view;

import java.io.IOException;

import isen.quiz.App;
import javafx.fxml.FXML;

/**
 * @author Philippe Duval
 * 
 * This is the controller of the home screen. It is very simple, and just handles the change to the quiz view
 *
 */
public class HomeScreenController {
	
	@FXML
	public void handleLaunchButton() throws IOException {
		// This is quite sparse : just load the next scene on click, and voilà!		
		App.setRoot("/isen/quiz/view/QuizzOverview");
	}
}
