package isen.quiz;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author Philippe Duval
 * 
 *         This is our main class. It extends Application, which is a JavaFX
 *         class, and thus implements the inherited method s
 *
 */
public class App extends Application {

	private static Scene scene;

	@Override
	public void start(Stage stage) throws IOException {
		// Put a fancy name on our window
		stage.setTitle("my beautiful title");
		// Load our first scene using the convenient method provided by JavaFX. Mind
		// your package hierarchy !
		scene = new Scene(loadFXML("/isen/quiz/view/HomeScreen"), 640, 480);
		// get the primary stage and put the first scene on the stage
		stage.setScene(scene);
		// Curtains up !
		stage.show();
	}

	public static void setRoot(String fxml) throws IOException {
		scene.setRoot(loadFXML(fxml));
	}

	private static Parent loadFXML(String fxml) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
		return fxmlLoader.load();
	}

	public static void main(String[] args) {
		launch();
	}

}
